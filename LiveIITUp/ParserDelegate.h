//
//  ParserDelegate.h
//  CS442Assignment1
//
//  Created by Eyoel Berhane Asfaw on 2/23/13.
//  Copyright (c) 2013 Eyoel Berhane Asfaw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventObject.h"

@interface ParserDelegate : NSObject <NSXMLParserDelegate>
{
    BOOL _foundTitle;
    BOOL _foundItemToFill;
    BOOL _fillElement;
    NSString *currentElementName;
    NSMutableDictionary *_elements;
    NSInteger *_elementNumber;
    EventObject *_feedObject;

}
@property (nonatomic,strong) NSMutableArray *feeds;

@end
