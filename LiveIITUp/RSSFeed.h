//
//  RSSFeed.h
//  Assignment2
//
//  Created by Eyoel Berhane Asfaw on 3/8/13.
//  Copyright (c) 2013 Eyoel Berhane Asfaw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParserDelegate.h"
#import "EventObject.h"

@interface RSSFeed : NSObject

+(NSArray *)parseFeedItemsforURL:(NSString *)URL;

@end
