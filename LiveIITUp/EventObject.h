//
//  FeedObject.h
//  CS442Assignment1
//
//  Created by Eyoel Berhane Asfaw on 3/4/13.
//  Copyright (c) 2013 Eyoel Berhane Asfaw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventObject : NSObject
@property (strong, nonatomic) NSString *title, *type, *description, *link, *date;
@property  (strong, nonatomic) NSMutableDictionary  *elements;

@end
