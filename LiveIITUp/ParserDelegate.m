//
//  ParserDelegate.m
//  CS442Assignment1
//
//  Created by Eyoel Berhane Asfaw on 2/23/13.
//  Copyright (c) 2013 Eyoel Berhane Asfaw. All rights reserved.
//

#import "ParserDelegate.h"


@implementation ParserDelegate
@synthesize feeds=_feeds;

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{

    if([elementName isEqualToString:@"title"] && _elementNumber==0){
        _foundTitle = YES;
    }else if([elementName isEqualToString:@"item"] || [elementName isEqualToString:@"entry"]){
        _feedObject = [[EventObject alloc] init];
        _elements = [[NSMutableDictionary alloc] init];
        currentElementName = nil;
        _elementNumber++;
        _foundItemToFill=YES;
        _fillElement=YES;
        
        
    }
    if(_foundItemToFill){
        if ([elementName isEqualToString:@"title"] || [elementName isEqualToString:@"link"] || [elementName isEqualToString:@"pubDate"] || [elementName isEqualToString:@"published"] || [elementName isEqualToString:@"description"] || [elementName isEqualToString:@"content"]) {
            currentElementName = [NSString stringWithString:elementName];
            _fillElement=YES;
            
        }else{
            _fillElement=NO;
        }
        
    }else{
        _fillElement=NO;
    }
    
    
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    if (_foundTitle) {
        [_feeds addObject:string];
    }else if(_fillElement){
        if (currentElementName) {
            [_elements setObject:[NSString stringWithString:string] forKey:[NSString stringWithString:currentElementName]];
            _fillElement=NO;
        }
        
    }

    
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{

    if ([elementName isEqualToString:@"title"]& !_elementNumber) {
        _foundTitle = NO;
        _elementNumber++;
    }else if([elementName isEqualToString:@"item"] || [elementName isEqualToString:@"entry"]){
        [_feedObject setElements:[NSDictionary dictionaryWithDictionary:_elements]];
        [_feeds addObject:_feedObject];
        _elementNumber++;
        _fillElement=NO;
        _foundItemToFill=NO;
    }
    
}
-(void)parserDidStartDocument:(NSXMLParser *)parser{
    _feeds = [[NSMutableArray alloc] init];
    _elementNumber = 0;
}
    


@end
