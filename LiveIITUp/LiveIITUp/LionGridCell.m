//
//  LionGridCell.m
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 1/19/13.
//  Copyright (c) 2013 Eyoel Berhane Asfaw. All rights reserved.
//

#import "LionGridCell.h"
//#import "IITEventsGridVC.h"


@implementation LionGridCell
@synthesize date, time, name, addr, description, promptMode;
@synthesize lionOverlay;
@synthesize image, backView, frontView, frontSide;
@synthesize icon1, icon2, icon3, icon4, icon5, icon6;
@synthesize eventInfo;
@synthesize myParent;
@synthesize mySLComposerSheet;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }

    return self;
}

- (IBAction)facebookButton:(id)sender {
//    [(id)myParent facebookButtonPressed:eventInfo];
    
    NSString *eventInfoString = [NSString stringWithFormat:@"I'm going to %@ , check it out on the UCal!", name.text];
    
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) //check if Facebook Account is linked
    {
        mySLComposerSheet = [[SLComposeViewController alloc] init]; //initiate the Social Controller
        mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook]; //Tell him with what social plattform to use it, e.g. facebook or twitter
        [mySLComposerSheet setInitialText:[NSString stringWithFormat:eventInfoString,mySLComposerSheet.serviceType]]; //the message you want to post
        //        [mySLComposerSheet addImage:yourimage]; //an image you could post
        //for more instance methodes, go here:https://developer.apple.com/library/ios/#documentation/NetworkingInternet/Reference/SLComposeViewController_Class/Reference/Reference.html#//apple_ref/doc/uid/TP40012205
        [myParent presentViewController:mySLComposerSheet animated:YES completion:nil];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:@"A Facebook account is not configured on this device's settings" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = @"Action Cancelled";
                break;
            case SLComposeViewControllerResultDone:
                output = @"Post Successfull";
                break;
            default:
                break;
        } //check if everythink worked properly. Give out a message on the state.
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }];
}


- (IBAction)addToCalendarButton:(id)sender {
    [(id)myParent setSelectedEvent:eventInfo];
    [myParent performSegueWithIdentifier:@"setReminderSegue" sender:myParent];
    
}
- (IBAction)emailAFriendButton:(id)sender {
    MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setSubject:@"Check out this event!"];
    [controller setMessageBody:[@"" stringByAppendingFormat:@"Hey, \n I saw this event %@ and thought you might like it. Here's a link to it : %@ \n\n powered by: tinyurl.com/LiveIITUp", name.text, @"SOme link" ] isHTML:NO];
    [myParent presentViewController:controller animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if (result == MFMailComposeResultSent)
       NSLog(@"Sent!");
    else
        NSLog(@"Not sent");
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
