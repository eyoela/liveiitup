//
//  EventDetailViewController.m
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/21/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//
#include "Global.c"
#import "DataFetcher.h"
#import "EventDetailViewController.h"
#import <EventKit/EventKit.h>

@interface EventDetailViewController ()

@end

@implementation EventDetailViewController
@synthesize navigationItem;
@synthesize eventLogo;
@synthesize eventTitle;
@synthesize chosenReminderTime;

@synthesize eventDate;
@synthesize eventOrganizer;
@synthesize eventLocation;
@synthesize infoIcon;
@synthesize eventInfo;
@synthesize addToCalButton;
@synthesize prizeIcon;
@synthesize sportIcon;
@synthesize proIcon;
@synthesize eduIcon;
@synthesize entertainIcon;
@synthesize foodIcon;

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self.addToCalButton setEnabled:YES];
}

- (IBAction)shareFB:(id)sender {
    NSString *eventInfoString = [NSString stringWithFormat:@"I'm going to %@ , check it out on the UCal!", self.eventTitle.text];
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) //check if Facebook Account is linked
    {
        mySLComposerSheet = [[SLComposeViewController alloc] init]; //initiate the Social Controller
        mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook]; //Tell him with what social plattform to use it, e.g. facebook or twitter
        [mySLComposerSheet setInitialText:[NSString stringWithFormat:eventInfoString,mySLComposerSheet.serviceType]]; //the message you want to post
//        [mySLComposerSheet addImage:yourimage]; //an image you could post
        //for more instance methodes, go here:https://developer.apple.com/library/ios/#documentation/NetworkingInternet/Reference/SLComposeViewController_Class/Reference/Reference.html#//apple_ref/doc/uid/TP40012205
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:@"A Facebook account is not configured on this device's settings" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = @"Action Cancelled";
                break;
            case SLComposeViewControllerResultDone:
                output = @"Post Successfull";
                break;
            default:
                break;
        } //check if everythink worked properly. Give out a message on the state.
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.eventInfo) {
        GDataWhen *when = [[self.eventInfo objectsForExtensionClass:[GDataWhen class]] objectAtIndex:0];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
////        //        [dateFormat setDateStyle:NSDateFormatterMediumStyle];
        [dateFormat setDateFormat:@"EEE, MMMM dd yyyy"];
        [dateFormat2 setDateFormat:@"hh:mma"];
        
        self.eventTitle.text=[[self.eventInfo title] stringValue];

        self.eventOrganizer.text=[@""  stringByAppendingFormat:@"%@",[[self.eventInfo content] stringValue]];
        self.eventLocation.text = [@"" stringByAppendingString:[[[self.eventInfo locations] objectAtIndex:0] stringValue]];
        self.eventDate.text =  [@"" stringByAppendingFormat:@"%@",[dateFormat stringFromDate:[[when startTime] date]]];
        self.eventTime.text=[@"" stringByAppendingFormat:@"%@",[dateFormat2 stringFromDate:[[when startTime] date]]];
        
        NSArray *stockImages = [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"imageNames"]];
        selectedPicture = arc4random() % 25;
        
        self.eventLogo.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[stockImages objectAtIndex:selectedPicture] ofType:@"jpg"]];
        
        
        NSString *tags = [[self.eventInfo content] stringValue];
        
        if ([tags rangeOfString:@"#education"].location == NSNotFound) {
            [self.eduIcon setHidden:YES];
        }if ([tags rangeOfString:@"#pro"].location == NSNotFound) {
            [self.proIcon setHidden:YES];
        }if ([tags rangeOfString:@"#entertainment"].location == NSNotFound) {
            [self.entertainIcon setHidden:YES];
        }if ([tags rangeOfString:@"#food"].location == NSNotFound) {
            [self.foodIcon setHidden:YES];
        }if ([tags rangeOfString:@"#prizes"].location == NSNotFound) {
            [self.prizeIcon setHidden:YES];
        }if ([tags rangeOfString:@"#sports"].location == NSNotFound) {
            [self.sportIcon setHidden:YES];
        }
        
    }
    //    self.eventTitle.text=[[self.eventInfo title] stringValue];

    
    
	// Do any additional setup after loading the view.
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sende{
    if ([segue.identifier isEqual: @"setAlert"]) {
        [segue.destinationViewController setSelectedEvent:self.eventInfo];
    }
}

- (void)viewDidUnload
{
    [self setNavigationItem:nil];
    [self setEventLogo:nil];
    [self setEventTitle:nil];
    [self setEventDate:nil];
    [self setEventOrganizer:nil];
    [self setInfoIcon:nil];
    [self setEventTitle:nil];
    [self setAddToCalButton:nil];
    [self setEventLocation:nil];
    [self setEventTime:nil];
    [self setScrollView:nil];
    [self setEventOrganizer:nil];
    [self setPrizeIcon:nil];
    [self setSportIcon:nil];
    [self setProIcon:nil];
    [self setEduIcon:nil];
    [self setEntertainIcon:nil];
    [self setFoodIcon:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end
