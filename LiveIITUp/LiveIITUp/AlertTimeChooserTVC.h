//
//  AlertTimeChoserTVC.h
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/20/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventDetailViewController.h"
#import "GData.h"

@interface AlertTimeChooserTVC : UITableViewController
@property (strong, nonatomic) NSArray *times;
@property (strong, atomic) NSArray *displayValues;
@property (readwrite, nonatomic) int selectedTime;
@property (strong, nonatomic) GDataEntryCalendarEvent *selectedEvent;
-(void)setAlarm:(NSInteger)chosenTime;
@end
