//
//  CalendarVC.m
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/21/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#include "Global.c"
#import "DataFetcher.h"
#import "CalendarTVC.h"
#import "ControlVariables.h"
#import "IITEventsCell.h"
#import "EventDetailViewController.h"
@interface CalendarTVC()
@property (strong, nonatomic) NSArray *stockImages;
@end

@implementation CalendarTVC
@synthesize username;
@synthesize searchBar;
@synthesize password;
@synthesize googleCalendarService;
@synthesize categories;
@synthesize categoryNames;
@synthesize category;
@synthesize stockImages;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.navigationController.navigationBar.tintColor = [UIColor darkGrayColor];
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //    [self.refreshControl addTarget:self
    //                            action:@selector(refresh)
    //                  forControlEvents:UIControlEventValueChanged];
    
    // Google Calendar Service
    googleCalendarService = [[GDataServiceGoogleCalendar alloc] init];
    [googleCalendarService setUserAgent:@"EyoelAsfaw-LiveIITUp-1.0"];
    [googleCalendarService setServiceShouldFollowNextLinks:NO];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem  alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                                                            target:self
                                                                                            action:@selector( refresh )];
    
        // Theming
    self.searchBar.delegate = (id)self;
    self.title = @"IIT UCal";
    
	self.navigationController.navigationBar.tintColor = lionNavBarBackgroundColor;
    
    UIImageView *bg;
    if (selectedCategory==0) {
        bg = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"loading" ofType:@"jpeg"]]];
    }else{
        bg = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"searching" ofType:@"jpeg"]]];
    }
    [self.tableView setBackgroundView:bg];
    [self.tableView.backgroundView setFrame:self.view.frame];
    
    
    NSLog(@"Value of categorySelected %d",selectedCategory);
    if (selectedCategory==1)
    {
        self.categories=[NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"orgtags"]];
        self.categoryNames= [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"organizations"]];
        NSLog(@"I Loaded the categories: %@",self.categories);
        NSLog(@"and the category index is: %d",categoryIndex);
    }if (selectedCategory==2)
    {
        self.categories=[NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"categorytags"]];
        self.categoryNames= [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"catagories"]];
        NSLog(@"I Loaded the categories: %@",self.categories);
        NSLog(@"and the category index is: %d",categoryIndex);
    }if (selectedCategory==3)
    {
        self.categories=[NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"locationtags"]];
        self.categoryNames= [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"locations"]];
        NSLog(@"I Loaded the categories: %@",self.categories);
        NSLog(@"and the category index is: %d",categoryIndex);
    }
    
    self.stockImages=[NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"imageNames"]];
    
    photoMatch = [[NSMutableDictionary alloc] init];
    
    [self refresh];
    
    [self.tableView reloadData];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView setContentOffset:CGPointMake(0, self.searchBar.bounds.size.height-5) animated:YES];
}

- (void)awakeFromNib
{
    [self.tableView setBackgroundColor:[UIColor blackColor]];
    self.tableView.rowHeight = lionCellHeight;
}



#pragma mark Google Data APIs

- (void)refresh
{
    //    [self.refreshControl beginRefreshing];
    [self.tableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"searching" ofType:@"jpeg"]]]];
    [eventList removeAllObjects];
    
    [googleCalendarService fetchPublicFeedWithURL:[NSURL URLWithString:@"https://www.google.com/calendar/feeds/calendar%40iit.edu/public/full?orderby=starttime&sortorder=ascending&futureevents=true&max-results=200"] feedClass:[GDataFeedCalendar class] delegate:self didFinishSelector:@selector(calendarsTicket:finishedWithFeed:error:)];
    
    //    [googleCalendarService fetchCalendarFeedForUsername:@"l1v3iitup@gmail.com"/*[AppDelegate appDelegate].username*/
    //                                               delegate:self
    //                                      didFinishSelector:@selector( calendarsTicket:finishedWithFeed:error: )];
    NSLog(@"refresh: sent request ->> %@",googleCalendarService);
}

- (void)handleError:(NSError *)error
{
    NSString *title, *msg;
    if( [error code]==kGDataBadAuthentication )
    {
        title = @"Authentication Failed";
        msg = @"Invalid username/password\n\nPlease go to the iPhone's settings to change your Google account credentials.";
    }
    else
    {
        // some other error authenticating or retrieving the GData object or a 304 status
        // indicating the data has not been modified since it was previously fetched
        title = @"Unknown Error";
        msg = [error localizedDescription];
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:msg
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)calendarsTicket:(GDataServiceTicket *)ticket finishedWithFeed:(GDataFeedCalendar *)feed error:(NSError *)error
{
    NSLog(@"Got a response for the events");
    //    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    //    [spinner startAnimating];
    //    UIBarButtonItem *refresh= self.navigationItem.rightBarButtonItem;
    
    //    if ([[self.navigationController.navigationBar subviews] count]>=2) {
    //Be careful with the next line, Here you get access to an static index,
    //Apple could change the structure of the navbar and your index may change in the future.
    //        [[[self.navigationController.navigationBar subviews] objectAtIndex:3] addSubview:spinner];
    
    //    }
    if( !error )
    {
        int count = [[feed entries] count];
        eventList = [[NSMutableArray alloc] init];
        for( int i=0; i<count; i++ )
        {
            if (selectedCategory==1) {
                if ([[[(GDataEntryCalendarEvent *)[[feed entries] objectAtIndex:i] content] stringValue] rangeOfString:[self.categories objectAtIndex:categoryIndex]].length) {
                    [eventList addObject:[[feed entries] objectAtIndex:i]];
                    //                    NSLog( @"Selected catagory %d", categoryIndex);
                }
            }else if (selectedCategory==2) {
                if ([[[(GDataEntryCalendarEvent *)[[feed entries] objectAtIndex:i] content] stringValue] rangeOfString:[self.categories objectAtIndex:categoryIndex]].length) {
                    [eventList addObject:[[feed entries] objectAtIndex:i]];
                }
            }else if (selectedCategory==3) {
                if ([[[[(GDataEntryCalendarEvent *)[[feed entries] objectAtIndex:i] locations] objectAtIndex:0] stringValue] rangeOfString:[self.categories objectAtIndex:categoryIndex]].length) {
                    [eventList addObject:[[feed entries] objectAtIndex:i]];
                    
                }
            }else{
                [eventList addObject:[[feed entries] objectAtIndex:i]];
            }
        }
        if ([eventList count]==0) {
            [self.tableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"noeventsfound" ofType:@"jpg"]]]];
        }else{
            //sort the events by time
            NSArray *sortedArray;
            sortedArray = [eventList sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                
                NSDate *dateA = [[[[a objectsForExtensionClass:[GDataWhen class]] objectAtIndex:0] startTime] date];
                NSDate *dateB = [[[[b objectsForExtensionClass:[GDataWhen class]] objectAtIndex:0] startTime] date];
                
                return [dateA compare:dateB];
            }];
            eventList = [NSMutableArray arrayWithArray:sortedArray];
        }
    }else
        [self handleError:error];
    
    [self.tableView reloadData];
    //    [spinner stopAnimating];
    //    [spinner removeFromSuperview];
}

-(void)viewDidLayoutSubviews{
    [super   viewDidLayoutSubviews];
    if (eventList.count) {
        int count = [eventList count];
        if (selectedCategory==0)
            self.title = [NSString stringWithFormat:@"IIT UCal (%i)", count];
        else if(self.categories)
            self.title = [NSString stringWithFormat:@"%@ (%i)",[self.categoryNames objectAtIndex:categoryIndex] ,count];
    }
    
}


#pragma mark Table Content and Appearance

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    //    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self.searchBar resignFirstResponder];
    //    self.tableView.allowsSelection = YES;
    self.tableView.scrollEnabled = YES;
    self.searchBar.text=@"";
    //    [self.tableView setContentOffset:CGPointMake(0, self.searchBar.bounds.size.height-5) animated:YES];
    //    isFiltered = FALSE;
    //    [self.tableView reloadData];
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        [self.searchBar setShowsCancelButton:YES animated:YES];
        isFiltered = true;
        filteredTableData = [[NSMutableArray alloc] init];
        
        
        
        for (GDataEntryCalendarEvent* event in eventList)
        {
            NSRange nameRange = [[[event title] stringValue]  rangeOfString:text options:NSCaseInsensitiveSearch];
            NSRange locationRange = [[[[event locations] objectAtIndex:0] stringValue] rangeOfString:text options:NSCaseInsensitiveSearch];
            NSRange descriptionRange = [[[event content] stringValue] rangeOfString:text options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound || locationRange.location != NSNotFound)
            {
                [filteredTableData addObject:event];
            }
        }
    }
    
    [self.tableView reloadData];
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if (isFiltered) {
        return 1;
    }
    return MAX( [eventList count], 1 );
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if( section>=[eventList count] )
        return 0;
    
    if (isFiltered) {
        return [filteredTableData count];
    }
    
    return [eventList count];
}
//method for making random numbers between 1-25
+(UIImage *)findPicturePlacement:(GDataEntryCalendarEvent *)indexPath{
    UIImage *imageView;
    
    int level = 25 / 25;
    if (level==1) {
        //coming soon
    }
    else{
        
    }
    if (30 >=25) {
        //coming soon
    }{
        
    }
    //    return arc4random() % 25; //for now, quick fix
    return imageView;
    
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    static NSString *CellIdentifier = @"DetailCell";
    IITEventsCell *cell;// = (IITEventsCell *)[tableView :CellIdentifier];
    //    if( !cell ){
    
    cell = [[IITEventsCell alloc] initWithFrame:CGRectMake(0, 0, lionCellWidth, lionCellHeight)];
    cell.accessoryType = UITableViewCellAccessoryNone;
    //    }
    
    cell.date.text = cell.time.text = cell.name.text = cell.addr.text = @"";
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    
    
    
    // The DetailCell has two modes of display - either the typical record or a prompt for creating a new item
    if( indexPath.row<[eventList count] ){
        
        GDataEntryCalendarEvent *event = [eventList objectAtIndex:indexPath.row];
        if (isFiltered) {
            event = [filteredTableData objectAtIndex:indexPath.row];
        }
        
        //pic an image and store the image info
        NSString   *title = [[event title] stringValue];
        int r;
        if (![photoMatch valueForKey:title]) {
            //choose random pictures from stockpile and set as background
            r =arc4random() % 25;
            [photoMatch setObject:[NSNumber numberWithInt:r] forKey:title];
        } else {
            r=[[photoMatch objectForKey:title] intValue];
        }
        
        
        cell.image.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[self.stockImages objectAtIndex:r] ofType:@"jpg"]];
        
        
        GDataWhen *when = [[event objectsForExtensionClass:[GDataWhen class]] objectAtIndex:0];
        if( when ){
            NSDate *date = [[when startTime] date];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            
            [dateFormatter setDateFormat:@"EEE, MMM dd"];
            cell.date.text = [dateFormatter stringFromDate:date];
            [dateFormatter setDateFormat:@"hh:mma"];
            cell.time.text = [dateFormatter stringFromDate:date];
            
        }
        cell.name.text = title;
        // Note: An event might have multiple locations.  We're only displaying the first one.
        GDataWhere *addr = [[event locations] objectAtIndex:0];
        if( addr )
            cell.addr.text = [addr stringValue];
        
        cell.promptMode = NO;
        
        
        //check for tags
        NSString *tags = [[event content] stringValue];
        
        if ([tags rangeOfString:@"education"].location == NSNotFound) {
            [cell.icon5 setHidden:YES];
        }if ([tags rangeOfString:@"entertain"].location == NSNotFound) {
            [cell.icon6 setHidden:YES];
        }if ([tags rangeOfString:@"pro"].location == NSNotFound) {
            [cell.icon4 setHidden:YES];
        }if ([tags rangeOfString:@"food"].location == NSNotFound) {
            [cell.icon2 setHidden:YES];
        }if ([tags rangeOfString:@"prize"].location == NSNotFound) {
            [cell.icon3 setHidden:YES];
        }if ([tags rangeOfString:@"athlet"].location == NSNotFound) {
            [cell.icon1 setHidden:YES];
        }
        //        CGRect frame = cell.frame;
        //        frame.size.height -=5;
        //        cell.frame = frame;
        
    }else{
        cell.name.text = @"Loading events...";
        cell.promptMode = YES;
    }
    
    
    
    
    
    return cell;
}


#pragma mark Editing

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"detailView"]) {
        
        //        NSLog(@"Object being set : %@",calanderEvent);
        [segue.destinationViewController setEventInfo:calanderEvent];
    }
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    calanderEvent=[[GDataEntryCalendarEvent alloc] init];
    calanderEvent=[eventList objectAtIndex:indexPath.row];
    
    NSLog(@"Print out of calendar event author: %@", [[calanderEvent authors] objectAtIndex:0]);
    
    if (isFiltered) {
        calanderEvent = [filteredTableData objectAtIndex:indexPath.row];
    }
    
    
    [self performSegueWithIdentifier:@"detailView" sender:self];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    // cancel all the pending tickets so that we don't get errors in the background
    //    for( int section=0; section<[data count]; section++ )
    //    {
    //        NSMutableDictionary *nextDictionary = [data objectAtIndex:section];
    //        GDataServiceTicket *nextTicket = [nextDictionary objectForKey:KEY_TICKET];
    //        [nextTicket cancelTicket];
    //        
    //    }
}

- (void)viewDidUnload {
    [self setSearchBar:nil];
    [super viewDidUnload];
}
@end