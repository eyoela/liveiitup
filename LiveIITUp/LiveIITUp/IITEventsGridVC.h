//
//  IITEventsGridVC.h
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 1/19/13.
//  Copyright (c) 2013 Eyoel Berhane Asfaw. All rights reserved.
//
#import "GData.h"
#import "LionGridCell.h"
#import <UIKit/UIKit.h>
#import "DataFetcher.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "GData.h"

// These keys are used to lookup elements in our dictionaries.
#define KEY_CALENDAR @"calendar"
#define KEY_EVENTS @"events"
#define KEY_TICKET @"ticket"
#define KEY_EDITABLE @"editable"

@interface IITEventsGridVC : UIViewController
{
    NSMutableArray *eventList;
    NSMutableArray *data;
    NSMutableArray *filteredTableData;
    BOOL isFiltered;
    GDataServiceGoogleCalendar *googleCalendarService;
    NSMutableDictionary *photoMatch;
    
}
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, retain) GDataServiceGoogleCalendar *googleCalendarService;
@property (nonatomic, readwrite) BOOL category;
@property (nonatomic, strong) NSArray *categories;
@property (nonatomic, strong) NSArray *categoryNames;
@property (nonatomic, strong) NSMutableDictionary *ImagesCollection;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UILabel *calendarTitle;
@property (strong, nonatomic) GDataEntryCalendarEvent *selectedEvent;
@property (strong, nonatomic) IBOutlet UIButton *greyBackground;
- (void)refresh;

- (void)performTransition:(UIViewAnimationOptions)options withCell:(UITableViewCell *)cell;
- (IBAction)unwindSegue:(UIStoryboardSegue *)segue;
- (IBAction)returnfromPopOverSegue:(UIStoryboardSegue *)segue;

@end
