//
//  DataFetcher.m
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/21/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

//#include "Global.c"
#import "Filter.h"
#import "DataFetcher.h"

@implementation DataFetcher

@synthesize username;
@synthesize password;
@synthesize googleCalendarService;

+(NSArray *)fetchArrayfromJsonFile:(NSString *)filePath
{
    NSData *dataFromFile = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:filePath ofType:@"json"]];
    NSError *errors;
    NSArray * arrayofValues=[NSJSONSerialization JSONObjectWithData:dataFromFile options:0 error:&errors];
    return arrayofValues;
}


-(void) initializeData
{
    
    [self refresh];
}

#pragma mark Google Data APIs
- (void)refresh
{
    [data removeAllObjects];
    [googleCalendarService fetchCalendarFeedForUsername:@"l1v3iitup@gmail.com"/*[AppDelegate appDelegate].username*/
                                               delegate:self
                                      didFinishSelector:@selector( calendarsTicket:finishedWithFeed:error: )];
    NSLog(@"refresh: sent request ->> %@",googleCalendarService);
}

- (void)handleError:(NSError *)error
{
    NSString *title, *msg;
    if( [error code]==kGDataBadAuthentication )
    {
        title = @"Authentication Failed";
        msg = @"Invalid username/password\n\nPlease go to the iPhone's settings to change your Google account credentials.";
    }
    else
    {
        // some other error authenticating or retrieving the GData object or a 304 status
        // indicating the data has not been modified since it was previously fetched
        title = @"Unknown Error";
        msg = [error localizedDescription];
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:msg
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)calendarsTicket:(GDataServiceTicket *)ticket finishedWithFeed:(GDataFeedCalendar *)feed error:(NSError *)error
{
    NSLog(@"Got a response");
    if( !error )
    {
        int count = [[feed entries] count];
        for( int i=0; i<count; i++ )
        {
            GDataEntryCalendar *calendar = [[feed entries] objectAtIndex:i];
            NSLog(@" Calendar name: -> %@",[[calendar title] stringValue]);
            if ([[[calendar title] stringValue] isEqualToString:@"IIT University Calendar"])
            {
                
                // Create a dictionary containing the calendar and the ticket to fetch its events.
                NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
                [data addObject:dictionary];
                
                [dictionary setObject:calendar forKey:KEY_CALENDAR];
                [dictionary setObject:[[NSMutableArray alloc] init] forKey:KEY_EVENTS];
                
                if( [calendar ACLLink] )  // We can determine whether the calendar is under user's control by the existence of its edit link.
                    [dictionary setObject:KEY_EDITABLE forKey:KEY_EDITABLE];
                
                NSURL *feedURL = [[calendar alternateLink] URL];
                if( feedURL ){
                    GDataQueryCalendar* query = [GDataQueryCalendar calendarQueryWithFeedURL:feedURL];
                    
                    // Currently, the app just shows calendar entries from 15 days ago to 31 days from now.
                    // Ideally, we would instead use similar controls found in Google Calendar web interface, or even iCal's UI.
                    NSDate *minDate = [NSDate date];  // From right now...
                    NSDate *maxDate = [NSDate dateWithTimeIntervalSinceNow:60*60*24*90];  // ...to 90 days from now.
                    
                    [query setMinimumStartTime:[GDataDateTime dateTimeWithDate:minDate timeZone:[NSTimeZone systemTimeZone]]];
                    [query setMaximumStartTime:[GDataDateTime dateTimeWithDate:maxDate timeZone:[NSTimeZone systemTimeZone]]];
                    [query setOrderBy:@"starttime"];  // http://code.google.com/apis/calendar/docs/2.0/reference.html#Parameters
                    [query setIsAscendingOrder:YES];
                    [query setShouldExpandRecurrentEvents:YES];
                    
                    GDataServiceTicket *ticket = [googleCalendarService fetchFeedWithQuery:query
                                                                                  delegate:self
                                                                         didFinishSelector:@selector( eventsTicket:finishedWithEntries:error: )];
                    // I add the service ticket to the dictionary to make it easy to find which calendar each reply belongs to.
                    [dictionary setObject:ticket forKey:KEY_TICKET];
                }
            }
        }
    }else
        [self handleError:error];
    
}

- (void)eventsTicket:(GDataServiceTicket *)ticket finishedWithEntries:(GDataFeedCalendarEvent *)feed error:(NSError *)error{
    if( !error )
    {
        NSMutableDictionary *dictionary;
        for( int section=0; section<[data count]; section++ )
        {
            NSMutableDictionary *nextDictionary = [data objectAtIndex:section];
            GDataServiceTicket *nextTicket = [nextDictionary objectForKey:KEY_TICKET];
            if( nextTicket==ticket )
            {		// We've found the calendar these events are meant for...
                dictionary = nextDictionary;
                break;
            }
        }
        
        
        if(!dictionary)
            return;
        else
            [[NSUserDefaults standardUserDefaults] setObject:dictionary forKey:@"globalDictionary"];
        
        
        int count = [[feed entries] count];
        NSMutableArray *events = [dictionary objectForKey:KEY_EVENTS];
        
        for( int i=0; i<count; i++ )
        {
            [events addObject:[[feed entries] objectAtIndex:i]];
        }
        
        NSURL *nextURL = [[feed nextLink] URL];
        if( nextURL ){    // There are more events in the calendar...  Fetch again.
            GDataServiceTicket *newTicket = [googleCalendarService fetchFeedWithURL:nextURL
                                                                           delegate:self
                                                                  didFinishSelector:@selector( eventsTicket:finishedWithEntries:error: )];
            [dictionary setObject:newTicket forKey:KEY_TICKET];
        }
    }
    else
        [self handleError:error];
}


@end