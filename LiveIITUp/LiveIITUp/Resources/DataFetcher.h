//
//  DataFetcher.h
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/21/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#import "GData.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define KEY_CALENDAR @"calendar"
#define KEY_EVENTS @"events"
#define KEY_TICKET @"ticket"
#define KEY_EDITABLE @"editable"


@interface DataFetcher : NSObject
{
    NSMutableArray *data;
    BOOL isFiltered;
    GDataServiceGoogleCalendar *googleCalendarService;
    GDataEntryCalendarEvent *calanderEvent;
}
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, retain) GDataServiceGoogleCalendar *googleCalendarService;

-(void) initializeData;
+(NSArray *)fetchArrayfromJsonFile:(NSString *)filePath;
@end
