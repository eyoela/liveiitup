//
//  IITEventsGridVC.m
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 1/19/13.
//  Copyright (c) 2013 Eyoel Berhane Asfaw. All rights reserved.
//
#include "Global.c"
#import "IITEventsGridVC.h"
#import <EventKit/EventKit.h>

@interface IITEventsGridVC ()
{
    SLComposeViewController *mySLComposerSheet;
}
@property(nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) NSArray *stockImageNames;

@end

@implementation IITEventsGridVC
@synthesize username;
@synthesize searchBar;
@synthesize password;
@synthesize googleCalendarService;
@synthesize categories;
@synthesize categoryNames;
@synthesize category;
@synthesize stockImageNames;
@synthesize ImagesCollection;
@synthesize activityIndicator;
@synthesize calendarTitle;
@synthesize selectedEvent;
@synthesize greyBackground;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)refreshButtonPressed:(id)sender {
    selectedCategory=0;
    [self refresh];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    // Theming
    // transparent search bar color
    [self.searchBar setBackgroundColor:[UIColor colorWithRed:0.63 green:0.07 blue:0.07 alpha:0.00]];
    [self.searchBar setBarStyle:UIBarStyleDefault];
    
    self.searchBar.delegate = (id)self;
    self.title = @"IIT UCal";
    
    // Google Calendar Service
    googleCalendarService = [[GDataServiceGoogleCalendar alloc] init];
    [googleCalendarService setUserAgent:@"EyoelAsfaw-LiveIITUp-1.0"];
    [googleCalendarService setServiceShouldFollowNextLinks:NO];
    data = [[NSMutableArray alloc] init];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem  alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                                                            target:self
                                                                                            action:@selector( refresh )];
    
    
    // Set Service Credentials
    [googleCalendarService setUserCredentialsWithUsername:@"l1v3iitup@gmail.com"    //appDelegate.username
                                                 password:@"kickin12"];
    
    NSLog(@"Value of categorySelected %d",selectedCategory);
    if (selectedCategory==1)
    {
        self.categories=[NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"orgtags"]];
        self.categoryNames= [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"organizations"]];
        NSLog(@"I Loaded the categories: %@",self.categories);
        NSLog(@"and the category index is: %d",categoryIndex);
    }if (selectedCategory==2)
    {
        self.categories=[NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"categorytags"]];
        self.categoryNames= [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"catagories"]];
        NSLog(@"I Loaded the categories: %@",self.categories);
        NSLog(@"and the category index is: %d",categoryIndex);
    }if (selectedCategory==3)
    {
        self.categories=[NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"locationtags"]];
        self.categoryNames= [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"locations"]];
        NSLog(@"I Loaded the categories: %@",self.categories);
        NSLog(@"and the category index is: %d",categoryIndex);
    }

    
    self.stockImageNames=[NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"iPadImageNames"]];
//    self.ImagesCollection = 
//    for (NSString *name in self.stockImageNames) {
//
//        [self.ImagesCollection setObject:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:name ofType:@"jpg"]] forKey:name];
//    }
    
    photoMatch = [[NSMutableDictionary alloc] init];
    
    
    [self refresh];
    
    //initialize activity indicator
//    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    [self.activityIndicator setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Utility methods for searching index paths.
- (NSDictionary *)dictionaryForIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.section<[data count] )
        return [data objectAtIndex:indexPath.section];
    return nil;
}

- (NSMutableArray *)eventsForIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dictionary = [self dictionaryForIndexPath:indexPath];
    if( dictionary )
        return [dictionary valueForKey:KEY_EVENTS];
    return nil;
}

- (GDataEntryCalendarEvent *)eventForIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *events = [self eventsForIndexPath:indexPath];
    if( events && indexPath.row<[events count] )
        return [events objectAtIndex:indexPath.row];
    return nil;
}

#pragma mark Google Data APIs

- (void)refresh
{
    [data removeAllObjects];
    
//    [self.greyBackground setUserInteractionEnabled:NO];
//    [self.greyBackground setHidden:NO];
    [self.activityIndicator startAnimating];
    
    NSLog(@"Value of categorySelected %d",selectedCategory);
    if (selectedCategory==0) {
        calendarTitle.text = @"Main Calendar";
    }
    if (selectedCategory==1)
    {
        self.categories=[NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"orgtags"]];
        self.categoryNames= [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"organizations"]];
        NSLog(@"I Loaded the categories: %@",self.categories);
        NSLog(@"and the category index is: %d",categoryIndex);
        calendarTitle.text = [@"" stringByAppendingFormat:@"%@ Events",[self.categoryNames objectAtIndex:categoryIndex]];
    }if (selectedCategory==2)
    {
        self.categories=[NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"categorytags"]];
        self.categoryNames= [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"catagories"]];
        calendarTitle.text = [@"" stringByAppendingFormat:@"%@ Events",[self.categoryNames objectAtIndex:categoryIndex]];
    }if (selectedCategory==3)
    {
        self.categories=[NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"locationtags"]];
        self.categoryNames= [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"locations"]];
        calendarTitle.text = [@"" stringByAppendingFormat:@"%@ Events",[self.categoryNames objectAtIndex:categoryIndex]];
    }
    
    [googleCalendarService fetchPublicFeedWithURL:[NSURL URLWithString:@"https://www.google.com/calendar/feeds/calendar%40iit.edu/public/full?orderby=starttime&sortorder=ascending&futureevents=true&max-results=200"] feedClass:[GDataFeedCalendar class] delegate:self didFinishSelector:@selector(calendarsTicket:finishedWithFeed:error:)];
    
//    [googleCalendarService fetchCalendarFeedForUsername:@"l1v3iitup@gmail.com"/*[AppDelegate appDelegate].username*/
//                                               delegate:self
//                                      didFinishSelector:@selector( calendarsTicket:finishedWithFeed:error: )];
    NSLog(@"refresh: sent request ->> %@",googleCalendarService);
}

- (void)handleError:(NSError *)error
{
    NSString *title, *msg;
    if( [error code]==kGDataBadAuthentication )
    {
        title = @"Authentication Failed";
        msg = @"Invalid username/password\n\nPlease go to the iPhone's settings to change your Google account credentials.";
    }
    else
    {
        // some other error authenticating or retrieving the GData object or a 304 status
        // indicating the data has not been modified since it was previously fetched
        title = @"Unknown Error";
        msg = [error localizedDescription];
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:msg
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)calendarsTicket:(GDataServiceTicket *)ticket finishedWithFeed:(GDataFeedCalendar *)feed error:(NSError *)error
{
    NSLog(@"Got a response");
 
    if( !error )
    {
        int count = [[feed entries] count];
        eventList = [[NSMutableArray alloc] init];
        for( int i=0; i<count; i++ )
        {
            if (selectedCategory==1) {
                if ([[[(GDataEntryCalendarEvent *)[[feed entries] objectAtIndex:i] content] stringValue] rangeOfString:[self.categories objectAtIndex:categoryIndex]].length) {
                    [eventList addObject:[[feed entries] objectAtIndex:i]];
                    //                    NSLog( @"Selected catagory %d", categoryIndex);
                }
            }else if (selectedCategory==2) {
                if ([[[(GDataEntryCalendarEvent *)[[feed entries] objectAtIndex:i] content] stringValue] rangeOfString:[self.categories objectAtIndex:categoryIndex]].length) {
                    [eventList addObject:[[feed entries] objectAtIndex:i]];
                }
            }else if (selectedCategory==3) {
                if ([[[[(GDataEntryCalendarEvent *)[[feed entries] objectAtIndex:i] locations] objectAtIndex:0] stringValue] rangeOfString:[self.categories objectAtIndex:categoryIndex]].length) {
                    [eventList addObject:[[feed entries] objectAtIndex:i]];
                    
                }
            }else{
                [eventList addObject:[[feed entries] objectAtIndex:i]];
            }
        }
        if ([eventList count]==0) {
//            [self.tableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"noeventsfound" ofType:@"jpg"]]]];
#warning need some kind of indicator for no event
        }else{
            //sort the events by time
            NSArray *sortedArray;
            sortedArray = [eventList sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                
                NSDate *dateA = [[[[a objectsForExtensionClass:[GDataWhen class]] objectAtIndex:0] startTime] date];
                NSDate *dateB = [[[[b objectsForExtensionClass:[GDataWhen class]] objectAtIndex:0] startTime] date];
                
                return [dateA compare:dateB];
            }];
            eventList = [NSMutableArray arrayWithArray:sortedArray];
            
        }
    }else
        [self handleError:error];

    [self.activityIndicator stopAnimating];
    [self.activityIndicator setHidden:YES];
//    [self.greyBackground setUserInteractionEnabled:YES];
//    [self.greyBackground setHidden:YES];
    [self.collectionView reloadData];
}


- (void)updateCalendarEvent:(GDataEntryCalendarEvent *)event{
    [googleCalendarService fetchEntryByUpdatingEntry:event
                                         forEntryURL:[[event editLink] URL]
                                            delegate:self
                                   didFinishSelector:nil];
}





#pragma mark - UICollectionView Datasource
// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    if( section>=[eventList count] )
        return 0;
    
    if (isFiltered) {
        return [filteredTableData count];
    }
    
    return [eventList count];
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    return 1 ;
}
// 3
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    LionGridCell *cell = (LionGridCell*)[cv dequeueReusableCellWithReuseIdentifier:@"EventCell" forIndexPath:indexPath];
    
    // clean up the cell
    
    cell.date.text = cell.time.text = cell.description.text = cell.name.text = cell.addr.text = @"";
    cell.frontView.image=nil;
    cell.description.hidden=YES;
    cell.detailDivider.hidden=YES;
    cell.facebookButton.hidden=YES;
    cell.calendarButton.hidden=YES;
    cell.flipBackButton.hidden=YES;
    cell.mailButton.hidden=YES;
    cell.frontSide = YES;
    
    if( indexPath.row<[eventList count] ){
        
        GDataEntryCalendarEvent *event = [eventList objectAtIndex:indexPath.row];
        if (isFiltered) {
            event = [filteredTableData objectAtIndex:indexPath.row];
        }
        
        //pick an image and store the image info
        NSString   *title = [[event title] stringValue];
        int r;
        if (![photoMatch valueForKey:title]) {
            //choose random pictures from stockpile and set as background
            r =arc4random() % (22 - 1) + 1;
            
            [photoMatch setObject:[NSNumber numberWithInt:r] forKey:title];
        } else {
            r=[[photoMatch objectForKey:title] intValue];
        }
        
//        ideal way of doing it
//        cell.frontView.image = [self.ImagesCollection objectForKey:[self.stockImageNames objectAtIndex:r]];
        cell.frontView.image = [UIImage imageNamed:[@"" stringByAppendingFormat:@"%d.jpg", r]];
//        cell.frontView.image = [UIImage imageNamed:@"19.jpg"]; // for testing with small size pictures
//        [cell.contentView sendSubviewToBack:cell.frontView];
//        [cell.contentView sendSubviewToBack:cell.backView];
        
        
        GDataWhen *when = [[event objectsForExtensionClass:[GDataWhen class]] objectAtIndex:0];
        if( when ){
            NSDate *date = [[when startTime] date];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            
            [dateFormatter setDateFormat:@"EEE, MMM dd"];
            cell.date.text = [dateFormatter stringFromDate:date];
            [dateFormatter setDateFormat:@"hh:mma"];
            cell.time.text = [dateFormatter stringFromDate:date];
        }
        cell.name.text = title;
        // Note: An event might have multiple locations.  We're only displaying the first one.
        cell.description.text = [@""  stringByAppendingFormat:@"%@",[[event content] stringValue]];
        GDataWhere *addr = [[event locations] objectAtIndex:0];
        if( addr )
            cell.addr.text = [addr stringValue];
        
        cell.promptMode = NO;
        
        //check for tags
        NSString *tags = [[event content] stringValue];
        
        cell.icon5.hidden = ([tags rangeOfString:@"education"].location == NSNotFound) ? YES : NO;
        cell.icon6.hidden = ([tags rangeOfString:@"entertain"].location == NSNotFound) ? YES : NO;
        cell.icon4.hidden = ([tags rangeOfString:@"professional"].location == NSNotFound) ? YES : NO;
        cell.icon3.hidden = ([tags rangeOfString:@"prize"].location == NSNotFound) ? YES : NO;
        cell.icon2.hidden = ([tags rangeOfString:@"food"].location == NSNotFound) ? YES : NO;
        cell.icon1.hidden = ([tags rangeOfString:@"athlet"].location == NSNotFound) ? YES : NO;
        
        cell.eventInfo = event;
        cell.myParent=self;
        
        cell.mySLComposerSheet = mySLComposerSheet;
        
    }else{
        cell.name.text = @"Loading events...";
        cell.promptMode = YES;
    }

    [cell.contentView sendSubviewToBack:cell.frontView];
    [cell.contentView sendSubviewToBack:cell.backView];
    cell.frontSide=YES;
    
    return cell;
}
// 4
/*- (UICollectionReusableView *)collectionView:
 (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
 {
 return [[UICollectionReusableView alloc] init];
 }*/


#pragma mark - UICollectionViewDelegate

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
//******** testing something out here... fix it later
    //    [self.searchBar setShowsCancelButton:NO animated:YES];
//    [self.searchBar resignFirstResponder];
    //    self.tableView.allowsSelection = YES;
//    self.se.scrollEnabled = YES;
    //    self.searchBar.text=@"";
    //    [self.tableView setContentOffset:CGPointMake(0, self.searchBar.bounds.size.height-5) animated:YES];
    
    //    isFiltered = FALSE;
    //    [self.tableView reloadData];
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
//        [self.searchBar setShowsCancelButton:YES animated:YES];
        isFiltered = true;
        filteredTableData = [[NSMutableArray alloc] init];
        
        for (GDataEntryCalendarEvent* event in eventList)
        {
            NSRange nameRange = [[[event title] stringValue]  rangeOfString:text options:NSCaseInsensitiveSearch];
            NSRange locationRange = [[[[event locations] objectAtIndex:0] stringValue] rangeOfString:text options:NSCaseInsensitiveSearch];
            NSRange descriptionRange = [[[event content] stringValue] rangeOfString:text options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound || locationRange.location != NSNotFound)
            {
                [filteredTableData addObject:event];
            }
        }
    }
    
    [self.collectionView reloadData];
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO: Select Item
    if ([collectionView cellForItemAtIndexPath:indexPath]) {

    }
    
    
    
    LionGridCell * cell= (LionGridCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    UIViewAnimationOptions transitionOptions = cell.frontSide ?
    UIViewAnimationOptionTransitionFlipFromLeft : UIViewAnimationOptionTransitionFlipFromRight;
    
    [self performTransition:transitionOptions withCell:(UITableViewCell*)cell];
    
}


- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Deselect item
}

#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    //    NSString *searchTerm = self.searches[indexPath.section]; FlickrPhoto *photo =
    //    self.searchResults[searchTerm][indexPath.row];
    // 2
    CGSize retval = CGSizeMake(245, 245);
    //    retval.height += 35; retval.width += 35;
    return retval;
}

// 3
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 5, 5, 5);
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView
      canPerformAction:(SEL)action
    forItemAtIndexPath:(NSIndexPath *)indexPath
            withSender:(id)sender {
    // Support only copying nad pasting of cells.
    if ([NSStringFromSelector(action) isEqualToString:@"copy:"]
        || [NSStringFromSelector(action) isEqualToString:@"paste:"] || [NSStringFromSelector(action) isEqualToString:@"cut:"])
        return YES;
    // Prevent all other actions.
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action
    forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender{
    
}


- (void)performTransition:(UIViewAnimationOptions)options withCell:(LionGridCell *)cell
{
    UIView *fromView, *toView;
//    cell.backView= [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tile-mockup-back" ofType:@"jpg"]]];
    
//    [cell.backView setFrame:cell.bounds];
//    [cell.contentView addSubview:cell.backView];
    
//    cell.frontView= [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tile-mockup" ofType:@"jpg"]]];
//    
//    [cell.frontView setFrame:cell.bounds];
//    [cell.contentView addSubview:cell.frontView];
    
    if (cell.frontSide)
    {
        fromView = cell.frontView;
        toView = cell.backView;
        cell.description.hidden=NO;
        [cell.contentView bringSubviewToFront:cell.description];
        cell.frontSide = NO;
    }
    else
    {
        cell.description.hidden=YES;
        fromView = cell.backView;
        toView = cell.frontView;
        [cell.contentView sendSubviewToBack:cell.frontView];
        [cell.contentView sendSubviewToBack:cell.backView];
        cell.frontSide = YES;
    }
    
    [UIView transitionFromView:fromView
                        toView:toView
                      duration:1.0
                       options:options
                    completion:^(BOOL finished) {
                        // animation completed
                        if (cell.frontSide)
                        {
                            [cell.contentView sendSubviewToBack:cell.frontView];
                            [cell.contentView sendSubviewToBack:cell.backView];
                            cell.description.hidden=YES;
                            cell.detailDivider.hidden=YES;
                            cell.facebookButton.hidden=YES;
                            cell.calendarButton.hidden=YES;
                            cell.flipBackButton.hidden=YES;
                            cell.mailButton.hidden=YES;
                        }
                        else
                        {
                            
                            [cell.contentView bringSubviewToFront:cell.backView];
                            [cell.contentView bringSubviewToFront:cell.detailDivider];
                            [cell.contentView bringSubviewToFront:cell.time];
                            [cell.contentView bringSubviewToFront:cell.addr];
                            [cell.contentView bringSubviewToFront:cell.description];
                            [cell.contentView bringSubviewToFront:cell.facebookButton];
                            [cell.contentView bringSubviewToFront:cell.mailButton];
                            [cell.contentView bringSubviewToFront:cell.calendarButton];
                            [cell.contentView bringSubviewToFront:cell.flipBackButton];
                            cell.description.hidden=NO;
                            cell.detailDivider.hidden=NO;
                            cell.facebookButton.hidden=NO;
                            cell.calendarButton.hidden=NO;
                            cell.flipBackButton.hidden=NO;
                            cell.mailButton.hidden=NO;
                        }
                    }];
    
    
}

- (IBAction)unwindSegue:(UIStoryboardSegue *)segue
{
    NSLog(@"Unwind segue happened");
//    ViewController2 *srcController = segue.sourceViewController;
//    _data = srcController.data;  // retrieve data from child controller
    
    for( int section=0; section<[data count]; section++ )
    {
        NSMutableDictionary *nextDictionary = [data objectAtIndex:section];
        GDataServiceTicket *nextTicket = [nextDictionary objectForKey:KEY_TICKET];
        [nextTicket cancelTicket];
    }

    [self refresh];
}

- (IBAction)returnfromPopOverSegue:(UIStoryboardSegue *)segue
{
    NSLog(@"returnfromPopOver segue happened");
    //    ViewController2 *srcController = segue.sourceViewController;
    //    _data = srcController.data;  // retrieve data from child controller
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqual:@"setReminderSegue"]){
        [segue.destinationViewController setSelectedEvent:selectedEvent];
    }
}

- (void)viewDidUnload {
    [self setToolbar:nil];
    [super viewDidUnload];
}
@end
