//
//  CategorySelectionVC.m
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/29/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#include "Global.c"
#import "CategorySelectionVC.h"

@interface CategorySelectionVC ()

@end

@implementation CategorySelectionVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)performChoice:(int)selectedIndex{
    categoryIndex=selectedIndex;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self performSegueWithIdentifier:@"unWindtoCal" sender:self];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSLog(@" Selected Category: %d", selectedCategory);
}
- (IBAction)selectFood:(id)sender {
    [self performChoice:2];

}
- (IBAction)selecFood:(id)sender {
    [self performChoice:2];

}
- (IBAction)selectentertain:(id)sender {
    [self performChoice:1];
}
- (IBAction)selecEntertainment:(id)sender {
    [self performChoice:1];
}
- (IBAction)selectPro:(id)sender {
    [self performChoice:4];
}
- (IBAction)selecPro:(id)sender {
    [self performChoice:4];
}
- (IBAction)selectEducation:(id)sender {
    [self performChoice:0];
}
- (IBAction)selecEdu:(id)sender {
    [self performChoice:0];
}
- (IBAction)selectPrize:(id)sender {
    [self performChoice:3];
}
- (IBAction)selecPrize:(id)sender {
    [self performChoice:3];
}
- (IBAction)selectSport:(id)sender {
    [self performChoice:5];
}
- (IBAction)selecSport:(id)sender {
    [self performChoice:5];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
