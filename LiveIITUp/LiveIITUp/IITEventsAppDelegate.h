//
//  IITEventsAppDelegate.h
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/20/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IITEventsAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
