//
//  CategorySelectionVC.h
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/29/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategorySelectionVC : UIViewController
-(void)performChoice:(int)selectedIndex;
@end
