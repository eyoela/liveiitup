//
//  IITEventsCell.m
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/20/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#import "IITEventsCell.h"

@implementation IITEventsCell

@synthesize date, time, name, addr, promptMode;
//@synthesize prompt;
@synthesize image;
@synthesize icon1, icon2, icon3, icon4, icon5, icon6;


- (id)initWithFrame:(CGRect)frame
{
  self=  [super initWithFrame:frame];
        // Initialize the labels, their fonts, colors, alignment, and background color.
        
        //create date label
        date = [[UILabel alloc] initWithFrame:CGRectZero];
        date.font = [UIFont fontWithName:@"Helvetica Neue Light" size:12];
        date.textColor = [UIColor whiteColor];
        date.textAlignment = NSTextAlignmentRight;
        date.backgroundColor = [UIColor clearColor];
    
        //time label
        time = [[UILabel alloc] initWithFrame:CGRectZero];
        time.font = [UIFont fontWithName:@"Helvetica Neue Light" size:12];
        time.textColor = [UIColor whiteColor];
        time.textAlignment = NSTextAlignmentRight;
        time.backgroundColor = [UIColor clearColor];
    
        
        name = [[UILabel alloc] initWithFrame:CGRectZero];
        name.font = [UIFont fontWithName:@"Helvetica Neue Light" size:18];
        name.textColor = [UIColor whiteColor];
        name.textAlignment = NSTextAlignmentLeft;
        name.backgroundColor = [UIColor clearColor];
        
        addr = [[UILabel alloc] initWithFrame:CGRectZero];
        addr.font = [UIFont fontWithName:@"Helvetica Neue Light" size:12];
        addr.textColor = [UIColor whiteColor];
        addr.textAlignment = NSTextAlignmentRight;
        addr.backgroundColor = [UIColor clearColor];
    
        prompt = [[UILabel alloc] initWithFrame:CGRectZero];
        prompt.font = [UIFont boldSystemFontOfSize:12];
        prompt.textColor = [UIColor darkGrayColor];
        prompt.backgroundColor = [UIColor lightGrayColor];
    
        lionOverlay = [[UILabel alloc] initWithFrame:CGRectZero];
        lionOverlay.backgroundColor = [UIColor blackColor];
    
        lionBackground = [[UILabel alloc] initWithFrame:CGRectZero];
        lionBackground.backgroundColor = [UIColor blackColor];

        image = [[UIImageView alloc] init];
    
        self.icon1 =[[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"soccer" ofType:@"png"]]];
        self.icon2 = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"food" ofType:@"png"]]];
        self.icon3 = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ribbon" ofType:@"png"]]];
        self.icon4 = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tie" ofType:@"png"]]];
        self.icon5 = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"book" ofType:@"png"]]];
        self.icon6 = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"masks" ofType:@"png"]]];
    
        // Add the labels to the content view of the cell.
        
        // Important: although UITableViewCell inherits from UIView, you should add subviews to its content view
        // rather than directly to the cell so that they will be positioned appropriately as the cell transitions
        // into and out of editing mode.

        [self.contentView addSubview:lionBackground];
        [self.contentView addSubview:image];
        [self.contentView addSubview:lionOverlay];
        [self.contentView addSubview:prompt];
        [self.contentView addSubview:date];
        [self.contentView addSubview:time];
        [self.contentView addSubview:name];
        [self.contentView addSubview:addr];
        [self.contentView addSubview:self.icon1];
        [self.contentView addSubview:self.icon2];
        [self.contentView addSubview:self.icon3];
        [self.contentView addSubview:self.icon4];
        [self.contentView addSubview:self.icon5];
        [self.contentView addSubview:self.icon6];
    
        //    self.autoresizesSubviews = YES;
    
    return self;

}

//- (void)setPromptMode:(BOOL)flag{  // Setting the prompt mode to YES hides the date/time/name/addr labels and shows the prompt label.
//    if( flag ){
//        date.hidden = YES;
//        time.hidden = YES;
//        name.hidden = YES;
//        addr.hidden = YES;
//        prompt.hidden = NO;
//    }else{
//        date.hidden = NO;
//        time.hidden = NO;
//        name.hidden = NO;
//        addr.hidden = NO;
//        prompt.hidden = YES;
//    }
//}

- (void)layoutSubviews{
    [super layoutSubviews];
    CGRect contentRect = self.contentView.bounds;
    lionBackground.frame= contentRect;
    contentRect.size.height -= 15;
//    CGFloat boundsX = contentRect.origin.x;
//    CGRect frame;
    
    // Start with a rect that is inset from the content view by 10 pixels on all sides.
//    CGRect baseRect = CGRectInset( self.contentView.bounds, 10, 10 );


    CGRect rect = contentRect;

    // Position each label with a modified version of the base rect.
    
    //background picture
    image.frame = rect;
    
    //black overlay to dim the image
    rect = contentRect;
    lionOverlay.frame = rect;
    [lionOverlay setAlpha:0.4];
    
    //gray title bar
    rect.size= CGSizeMake(320, 45);
    prompt.frame = rect;
    [prompt setAlpha:0.7];

    // title
    rect = contentRect;
    rect.size.height = 37;
    rect.origin.x +=3;
    name.frame = rect;
    
    //location
    rect = contentRect;
    rect.size.height = 20;
    rect.origin.y += 80;
    addr.frame = rect;

    //time
    rect.origin.y +=20;
    time.frame=rect;
    
    //date
    rect = contentRect;
    rect.size.height = 20;
    rect.origin.y += 25;
    date.frame=rect;
    
    //icon1
    rect.size = CGSizeMake(35, 35);
    rect.origin.x =285;
    rect.origin.y = 45;
    self.icon1.frame = rect;
    
    //icon2
    rect.origin.x -=35;
    self.icon2.frame = rect;
    
    //icon3
    rect.origin.x -=35;
    self.icon3.frame = rect;
    
    //icon4
    rect.origin.x -=35;
    self.icon4.frame = rect;
    
    //icon5
    rect.origin.x -=35;
    self.icon5.frame = rect;
    
    //icon6
    rect.origin.x -=35;
    self.icon6.frame = rect;
//    rect.size.width = baseRect.size.width - 60;
//    rect.origin.x += 60;
//    rect.origin.y -= 15;
//    name.frame = rect;
//    rect.origin.y += 14;
//    addr.frame = rect;
}

// Update the text color of each label when entering and exiting selected mode.
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated{
//    [super setSelected:selected animated:animated];
//    if( selected )
//        date.textColor = time.textColor = name.textColor = addr.textColor = prompt.textColor = [UIColor whiteColor];
//    else{
//        date.textColor = [UIColor darkGrayColor];
//        time.textColor = [UIColor darkGrayColor];
//        name.textColor = [UIColor blackColor];
//        addr.textColor = [UIColor darkGrayColor];
//        prompt.textColor = [UIColor darkGrayColor];
//    }
//}

@end