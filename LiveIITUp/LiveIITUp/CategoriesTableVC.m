//
//  CategoriesTableVC.m
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/23/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#include "Global.c"
#import "CalendarTVC.h"
#import "DataFetcher.h"
#import "CategoriesTableVC.h"

@interface CategoriesTableVC ()
@property (readwrite, nonatomic) bool isFiltered;
@end

@implementation CategoriesTableVC
@synthesize displayValues=_displayValues;
@synthesize isFiltered;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    self.searchBar.delegate = (id)self;
    [self.tableView setContentOffset:CGPointMake(0, self.searchBar.bounds.size.height-5) animated:YES];
    
    if (selectedCategory==1) {
        self.displayValues = [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"organizations"]];
        self.title = @"Organizations";
    }else if (selectedCategory==2)
    {
        self.displayValues = [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"catagories"]];
        self.title = @"Categories";
    }else if (selectedCategory==3)
    {
        self.displayValues = [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"locations"]];
        self.title = @"Locations";
    }
    
    
    
    NSLog(@"value of the displayValues: %@ testing one two", self.displayValues);
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.searchBar.delegate = (id)self;
    [self.tableView setContentOffset:CGPointMake(0, self.searchBar.bounds.size.height-5) animated:YES];
    
    if (selectedCategory==1) {
        self.displayValues = [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"organizations"]];
        self.title = @"Organizations";
    }else if (selectedCategory==2)
    {
        self.displayValues = [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"catagories"]];
        self.title = @"Categories";
    }else if (selectedCategory==3)
    {
        self.displayValues = [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"locations"]];
        self.title = @"Locations";
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rowCount;
    if(self.isFiltered)
        rowCount = self.filteredData.count;
    else
        rowCount = self.displayValues.count;
    
    return rowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if(isFiltered)
        cell.textLabel.text = [NSString stringWithString:[self.filteredData objectAtIndex:indexPath.row]];
    else
        cell.textLabel.text = [NSString stringWithString:[self.displayValues objectAtIndex:indexPath.row]];
    
    return cell;
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        [self.searchBar setShowsCancelButton:YES animated:YES];
        isFiltered = true;
        self.filteredData = [[NSMutableArray alloc] init];
        
        for (NSString* textValue in self.displayValues)
        {
            NSRange nameRange = [textValue rangeOfString:text options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound)
            {
                [self.filteredData addObject:textValue];
            }
        }
    }
    
    [self.tableView reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self.searchBar resignFirstResponder];
    self.tableView.allowsSelection = YES;
    self.tableView.scrollEnabled = YES;
    self.searchBar.text=@"";
    [self.tableView setContentOffset:CGPointMake(0, self.searchBar.bounds.size.height-5) animated:YES];
    
    isFiltered = FALSE;
    [self.tableView reloadData];
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (self.isFiltered)
        categoryIndex =  [self.displayValues indexOfObject:[self.filteredData objectAtIndex:indexPath.row]];
    else
        categoryIndex = (int)indexPath.row;
    
    NSLog(@"set Index Value: %d" , categoryIndex);
    //    selectedCategory = 1;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        [self performSegueWithIdentifier:@"CatagorySelectedSegue" sender:self];
    }else{
        [self performSegueWithIdentifier:@"unwindtoCalendar" sender:self];
    }
    
    
    
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    NSLog(@"i'm in seque prep!!");
    if ([segue.identifier isEqualToString:@"CatagorySelectedSegue"] )
    {
        NSLog(@"setting the value:!!");
        
    }
}

- (void)viewDidUnload {
    [self setSearchBar:nil];
    [super viewDidUnload];
}
@end
