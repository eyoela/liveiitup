//
//  IITEventsCell.h
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/20/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IITEventsCell : UITableViewCell{
UILabel *date, *time, *name, *addr;
UILabel *prompt;
UILabel *lionOverlay;
UILabel *lionBackground;
//UIImageView *image;
UIImageView  *icon2, *icon3, *icon4, *icon5, *icon6;
UIImageView *icon1;
BOOL promptMode;
}

@property (readwrite, retain) UILabel *date, *time, *name, *addr;
@property (strong, nonatomic) UIImageView *icon1, *icon2, *icon3, *icon4, *icon5, *icon6;
//@property (readonly, retain) UILabel *prompt;
@property (nonatomic) BOOL promptMode;
@property (readonly, retain) UIImageView *image;


@end
