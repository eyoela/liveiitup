//
//  AlertTimeChoserTVC.m
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/20/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

//
//  AlertTimeChooserTVC.m
//  WhatsUpIIT
//
//  Created by Eyoel Berhane Asfaw on 8/18/12.
//  Copyright (c) 2012 IIT. All rights reserved.
//
#include "Global.c"
#import "AlertTimeChooserTVC.h"
#import "DataFetcher.h"
#import "EventDetailViewController.h"
#import <EventKit/EventKit.h>

@interface AlertTimeChooserTVC ()

@end

@implementation AlertTimeChooserTVC
@synthesize times=_times;
@synthesize displayValues=_displayValues;
@synthesize selectedTime=_selectedTime;
@synthesize selectedEvent;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)doneButton:(id)sender {
    if (!self.selectedTime) {
        self.selectedTime=[[NSNumber numberWithInt:(-3600)] intValue];
    }
    selectedTimer = self.selectedTime;
    NSLog(@"Time interval passed to ParentVC: %d", self.selectedTime); 
    [self setAlarm:self.selectedTime];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone){
        [[self navigationController] popViewControllerAnimated:YES];
    }else{
        [self performSegueWithIdentifier:@"returnfromPopOver" sender:self];
    }

}
- (IBAction)cancelButton:(id)sender {
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone){
        [[self navigationController] popViewControllerAnimated:YES];
    }else{
        [self performSegueWithIdentifier:@"returnfromPopOver" sender:self];
        
    }
}

-(void)setAlarm:(NSInteger)chosenTime{
    
//    [self.addToCalButton setEnabled:NO];
    NSLog(@"about to add event to calendar");
    
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        NSLog(@"Access to calendar granted");
        /* This code will run when user has made his/her choice */
        EKEvent *event  = [EKEvent eventWithEventStore:eventStore];
        event.title     = [[self.selectedEvent title] stringValue];
        event.location = [[[self.selectedEvent locations] objectAtIndex:0] stringValue];
        
        GDataWhen *when = [[self.selectedEvent objectsForExtensionClass:[GDataWhen class]] objectAtIndex:0];
        
        event.startDate = [[when startTime] date];
        event.endDate   = [[when endTime] date];
        
        NSLog(@"ParentVC time interval: %d", chosenTime);
        if (!self.selectedTime){
            self.selectedTime = -3600;
            NSLog(@"The chosen time is not set!");
            //            chosenTime=self.chosenReminderTime;
        }
        
        event.alarms = [NSArray arrayWithObjects:[EKAlarm alarmWithRelativeOffset:self.selectedTime], nil];
        NSLog(@"the alarm was set to: %@",event.alarms);
        
        [event setCalendar:[eventStore defaultCalendarForNewEvents]];
        NSError *err;
        [eventStore saveEvent:event span:EKSpanThisEvent error:&err];
    }];
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.displayValues = [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"reminderDisplayValueList"]];
    
    
    NSLog(@"value of the displayValues: %@ testing one two", self.displayValues);
    
    self.times = [NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"reminderTimesList"]];
    NSLog(@"value of the times: %@ testing three four", self.times);
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"number of display values: %d", self.displayValues.count);
    // Return the number of rows in the section.
    return [self.displayValues count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    
    
    // Configure the cell...
    cell.textLabel.text = [NSString stringWithString:[self.displayValues objectAtIndex:indexPath.row]];
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    self.selectedTime =[[self.times objectAtIndex:indexPath.row] intValue];
    self.selectedTime =0 -self.selectedTime;
    NSLog(@"Currently selected time interval: %d", self.selectedTime);
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    
}

@end
