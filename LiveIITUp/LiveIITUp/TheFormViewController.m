//
//  TheFormViewController.m
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/27/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#import "TheFormViewController.h"

@interface TheFormViewController ()

@end

@implementation TheFormViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSString *urlAddress = @"http://www.theformiit.weebly.com";
    
    //Create a URL object.
    NSURL *url = [NSURL URLWithString:urlAddress];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    //Load the request in the UIWebView.
    [self.theForm loadRequest:requestObj];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTheForm:nil];
    [super viewDidUnload];
}
@end
