//
//  ControlVariables.h
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/23/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// iPhone CONSTANTS
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Width of eventlist table cells
#define lionTableLength                                320

// Width of eventlist table cells
#define lionCellWidth                                  320

// Height of the cells for eventlist table
#define lionCellHeight                                 135

// Padding for the eventlist cells  --- not in use yet
#define lionArticleCellVerticalInnerPadding            3
#define lionArticleCellHorizontalInnerPadding          3



// Vertical padding for the embedded table view within the row
#define lionRowVerticalPadding                         0
// Horizontal padding for the embedded table view within the row
#define lionRowHorizontalPadding                       0

// The background color of the eventlist table view
#define lionTableBackgroundColor               [UIColor colorWithRed:0.58823529 green:0.58823529 blue:0.58823529 alpha:1.0]

// Background color for navigation bar
#define lionNavBarBackgroundColor             [UIColor redColor]

// The background color on the horizontal table view for when we select a particular cell
#define lionTableSelectedBackgroundColor     [UIColor colorWithRed:0.0 green:0.59607843 blue:0.37254902 alpha:1.0]

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// iPad CONSTANTS
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Width (or length before rotation) of the table view embedded within another table view's row
#define kTableLength_iPad                               768

// Height for the Headlines section of the main (vertical) table view
#define kHeadlinesSectionHeight_iPad                    65

// Height for regular sections in the main table view
#define kRegularSectionHeight_iPad                      36

// Width of the cells of the embedded table view (after rotation, which means it controls the rowHeight property)
#define kCellWidth_iPad                                 192
// Height of the cells of the embedded table view (after rotation, which would be the table's width)
#define kCellHeight_iPad                                192

// Padding for the Cell containing the article image and title
#define kArticleCellVerticalInnerPadding_iPad           4
#define kArticleCellHorizontalInnerPadding_iPad         4

// Vertical padding for the embedded table view within the row
#define kRowVerticalPadding_iPad                        0
// Horizontal padding for the embedded table view within the row
#define kRowHorizontalPadding_iPad                      0
