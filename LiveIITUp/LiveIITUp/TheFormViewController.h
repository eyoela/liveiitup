//
//  TheFormViewController.h
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/27/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TheFormViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *theForm;

@end
