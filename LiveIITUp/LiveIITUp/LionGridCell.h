//
//  LionGridCell.h
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 1/19/13.
//  Copyright (c) 2013 Eyoel Berhane Asfaw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "GData.h"
#import "IITEventsGridVC.h"

@interface LionGridCell : UICollectionViewCell <MFMailComposeViewControllerDelegate, UINavigationControllerDelegate>
{
//    UILabel *date, *time, *name, *addr;
//    UILabel *prompt;
//    UITextView *description;
//    UIImageView *lionOverlay;
//    UILabel *lionBackground;
//    //UIImageView *image;
//    UIImageView  *icon2, *icon3, *icon4, *icon5, *icon6;
//    UIImageView *icon1, *frontView, *backView;
//    BOOL promptMode;
    BOOL frontSide;
    
}
@property (retain, nonatomic) IBOutlet UIImageView *frontView;
@property (retain, nonatomic) IBOutlet UIImageView *backView;
@property (readwrite, nonatomic) BOOL frontSide;
@property (strong, nonatomic) IBOutlet UIImageView *lionOverlay;
@property (strong, nonatomic) IBOutlet UILabel *date;
@property (strong, nonatomic) IBOutlet UILabel *time;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *addr;
@property (strong, nonatomic) IBOutlet UITextView *description;
@property (strong, nonatomic) IBOutlet UIImageView *eduIcon;
@property (strong, nonatomic) IBOutlet UIImageView *prizeIcon;
@property (strong, nonatomic) IBOutlet UIImageView *funIcon;
@property (strong, nonatomic) IBOutlet UIImageView *sportIcon;
@property (strong, nonatomic) IBOutlet UIImageView *foodIcon;
@property (strong, nonatomic) IBOutlet UIImageView *detailDivider;
@property (strong, nonatomic) IBOutlet UIButton *facebookButton;
@property (strong, nonatomic) IBOutlet UIButton *calendarButton;
@property (strong, nonatomic) IBOutlet UIButton *mailButton;
@property (strong, nonatomic) IBOutlet UIButton *flipBackButton;
@property (strong, nonatomic) IBOutlet UIImageView *icon2;
@property (strong, nonatomic) IBOutlet UIImageView *icon1;
@property (strong, nonatomic) IBOutlet UIImageView *icon3;
@property (strong, nonatomic) IBOutlet UIImageView *icon5;
@property (strong, nonatomic) IBOutlet UIImageView *icon6;
@property (strong, nonatomic) IBOutlet UIImageView *icon4;
@property (retain, nonatomic) GDataEntryCalendarEvent *eventInfo;
@property (strong, nonatomic) UIViewController  *myParent;
@property (strong, nonatomic) SLComposeViewController *mySLComposerSheet;

//@property (readonly, retain) UILabel *prompt;
@property (nonatomic) BOOL promptMode;
@property (readonly, retain) UIImageView *image;


@end
