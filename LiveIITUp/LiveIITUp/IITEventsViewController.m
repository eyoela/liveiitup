//
//  IITEventsViewController.m
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/20/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#include "Global.c"
#import "IITEventsViewController.h"

@interface IITEventsViewController ()

@end

@implementation IITEventsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"perform the clear of category selected");
    selectedCategory=0;
    categoryIndex=0;
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"navbar" ofType:@"png"]] forBarMetrics:UIBarMetricsDefault];
//    UIImage *tabBackground = [[UIImage imageNamed:@"navbar"]
//                              resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
//    [self.navigationController.navigationBar setBackgroundImage:tabBackground forBarMetrics:0];
    self.navigationController.navigationBar.tintColor = [UIColor redColor];

}
-(void)viewDidLayoutSubviews{
    selectedCategory=0;
    categoryIndex=selectedCategory=0;
}

- (IBAction)allEvents:(id)sender {
    selectedCategory=0;
}
- (IBAction)OrgMenu:(id)sender {
    selectedCategory=1;
}

- (IBAction)categoryMenu:(id)sender {
    selectedCategory=2;
}


- (IBAction)locationMenu:(id)sender {
    selectedCategory= 3;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)getFeedBack:(id)sender {
    NSURL *url = [NSURL URLWithString:@"https://docs.google.com/spreadsheet/viewform?formkey=dFdzUGl3bG9NMjNTMENhY2stdzZBNVE6MQ#gid=0"];
    
    if (![[UIApplication sharedApplication] openURL:url])
        
        NSLog(@"%@%@",@"Failed to open url:",[url description]);
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


@end
