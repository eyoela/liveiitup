//
//  EventDetailViewController.h
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/21/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "GData.h"
#import "AlertTimeChooserTVC.h"

#import "GData.h"


@interface EventDetailViewController : UIViewController
{
    SLComposeViewController *mySLComposerSheet;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITextView *eventOrganizer;

@property (strong, nonatomic) IBOutlet UINavigationItem *navigationItem;
@property (strong, nonatomic) IBOutlet UILabel *eventTime;
@property (strong, nonatomic) IBOutlet UIImageView *eventLogo;
@property (retain, nonatomic) IBOutlet UILabel *eventTitle;
@property (strong, nonatomic) IBOutlet UILabel *eventDate;
@property (weak, nonatomic) IBOutlet UILabel *eventLocation;
@property (strong, nonatomic) IBOutlet UIImageView *infoIcon;
@property (readwrite, nonatomic) NSInteger chosenReminderTime;
@property (retain, nonatomic) GDataEntryCalendarEvent *eventInfo;
@property (strong, nonatomic) IBOutlet UIImageView *prizeIcon;
@property (strong, nonatomic) IBOutlet UIImageView *sportIcon;
@property (strong, nonatomic) IBOutlet UIImageView *proIcon;
@property (strong, nonatomic) IBOutlet UIImageView *eduIcon;
@property (strong, nonatomic) IBOutlet UIImageView *entertainIcon;
@property (strong, nonatomic) IBOutlet UIImageView *foodIcon;
@property (weak, nonatomic) IBOutlet UIButton *addToCalButton;

@end