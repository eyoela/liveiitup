//
//  CalendarVC.h
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/21/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//
#import "GData.h"
#import "IITEventsViewController.h"
#import <UIKit/UIKit.h>


// These keys are used to lookup elements in our dictionaries.
#define KEY_CALENDAR @"calendar"
#define KEY_EVENTS @"events"
#define KEY_TICKET @"ticket"
#define KEY_EDITABLE @"editable"


@interface CalendarTVC : UITableViewController
{
    NSMutableArray *eventList;
    NSMutableArray *filteredTableData;
    BOOL isFiltered;
    GDataServiceGoogleCalendar *googleCalendarService;
    GDataEntryCalendarEvent *calanderEvent;
    NSMutableDictionary *photoMatch;
    
}
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, retain) GDataServiceGoogleCalendar *googleCalendarService;
@property (nonatomic, readwrite) BOOL category;
@property (nonatomic, strong) NSArray *categories;
@property (nonatomic, strong) NSArray *categoryNames;

- (void)refresh;
- (void)calendarsTicket:(GDataServiceTicket *)ticket finishedWithFeed:(GDataFeedCalendar *)feed error:(NSError *)error;
//- (void)insertCalendarEvent:(GDataEntryCalendarEvent *)event toCalendar:(GDataEntryCalendar *)calendar;
//- (void)updateCalendarEvent:(GDataEntryCalendarEvent *)event;
+(UIImage *)findPicturePlacement:(GDataEntryCalendarEvent *)indexPath;


@end
