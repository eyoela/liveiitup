//
//  RSSFeed.m
//  Assignment2
//
//  Created by Eyoel Berhane Asfaw on 3/8/13.
//  Copyright (c) 2013 Eyoel Berhane Asfaw. All rights reserved.
//

#import "RSSFeed.h"

@implementation RSSFeed


+(NSArray *)parseFeedItemsforURL:(NSString *)URL{
    NSURL *url = [NSURL URLWithString:URL];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    ParserDelegate *parserdelegate = [[ParserDelegate alloc] init];
    parser.delegate = parserdelegate;
    
    NSLog(@"Parsing enabled and completed");
    
    [parser  parse];
 
    NSMutableArray *feeds = [(id)parser.delegate feeds];
    if ([feeds count]<1) {
        NSLog(@" This Feed address is not valid");
        return @[];
    }else{
//        NSLog(@"Found %lu stories; Limited to the most recent %@:",[feeds count]-1, max);
        NSLog(@"**** Title of the RSS Feed: %@ ****",feeds[0]);
        return feeds;
    }
  
}

@end
