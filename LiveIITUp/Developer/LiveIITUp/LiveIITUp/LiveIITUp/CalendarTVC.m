//
//  CalendarVC.m
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/21/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#include "Global.c"
#import "DataFetcher.h"
#import "CalendarTVC.h"
#import "ControlVariables.h"
#import "IITEventsCell.h"
#import "EventDetailViewController.h"

@implementation CalendarTVC
@synthesize username;
@synthesize searchBar;
@synthesize password;
@synthesize googleCalendarService;
@synthesize categories;
@synthesize category;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.navigationController.navigationBar.tintColor = [UIColor darkGrayColor];
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Theming
    self.searchBar.delegate = (id)self;
    self.title = @"IIT UCal";
    [self.tableView setContentOffset:CGPointMake(0, self.searchBar.bounds.size.height-5) animated:YES];
	self.navigationController.navigationBar.tintColor = lionNavBarBackgroundColor;
    
    // Google Calendar Service
    googleCalendarService = [[GDataServiceGoogleCalendar alloc] init];
    [googleCalendarService setUserAgent:@"EyoelAsfaw-WhatsUpIIT-1.0"];
    [googleCalendarService setServiceShouldFollowNextLinks:NO];
    data = [[NSMutableArray alloc] init];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem  alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                                                            target:self
                                                                                            action:@selector( refresh )];

    
    // Set Service Credentials
    [googleCalendarService setUserCredentialsWithUsername:@"l1v3iitup@gmail.com"    //appDelegate.username
                                                 password:@"sears2012"];
    
    NSLog(@"Value of categorySelected %d",categorySelected);
    if (categorySelected==1)
    {
        self.categories=[NSArray arrayWithArray:[DataFetcher fetchArrayfromJsonFile:@"categorytags"]];
        NSLog(@"I Loaded the categories: %@",self.categories);
        NSLog(@"and the category index is: %d",categoryIndex);
    }
    
    [self refresh];
}

- (void)awakeFromNib
{
    [self.tableView setBackgroundColor:[UIColor blackColor]];
    self.tableView.rowHeight = lionCellHeight;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
}


#pragma mark Utility methods for searching index paths.
- (NSDictionary *)dictionaryForIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.section<[data count] )
        return [data objectAtIndex:indexPath.section];
    return nil;
}

- (NSMutableArray *)eventsForIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dictionary = [self dictionaryForIndexPath:indexPath];
    if( dictionary )
        return [dictionary valueForKey:KEY_EVENTS];
    return nil;
}

- (GDataEntryCalendarEvent *)eventForIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *events = [self eventsForIndexPath:indexPath];
    if( events && indexPath.row<[events count] )
        return [events objectAtIndex:indexPath.row];
    return nil;
}

#pragma mark Google Data APIs

- (void)refresh
{
    [data removeAllObjects];
    
    [googleCalendarService fetchCalendarFeedForUsername:@"eyoela@gmail.com"/*[AppDelegate appDelegate].username*/
                                               delegate:self
                                      didFinishSelector:@selector( calendarsTicket:finishedWithFeed:error: )];
    NSLog(@"refresh: sent request ->> %@",googleCalendarService);
}

- (void)handleError:(NSError *)error
{
    NSString *title, *msg;
    if( [error code]==kGDataBadAuthentication )
    {
        title = @"Authentication Failed";
        msg = @"Invalid username/password\n\nPlease go to the iPhone's settings to change your Google account credentials.";
    }
    else
    {
        // some other error authenticating or retrieving the GData object or a 304 status
        // indicating the data has not been modified since it was previously fetched
        title = @"Unknown Error";
        msg = [error localizedDescription];
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:msg
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)calendarsTicket:(GDataServiceTicket *)ticket finishedWithFeed:(GDataFeedCalendar *)feed error:(NSError *)error
{
    NSLog(@"Got a response");
    if( !error )
    {
        int count = [[feed entries] count];
        for( int i=0; i<count; i++ )
        {
            GDataEntryCalendar *calendar = [[feed entries] objectAtIndex:i];
#pragma come back here and add code
            NSLog(@" Calendar name: -> %@",[[calendar title] stringValue]);
            if ([[[calendar title] stringValue] isEqualToString:@"IIT University Calendar"])
            {
                
                // Create a dictionary containing the calendar and the ticket to fetch its events.
                NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
                [data addObject:dictionary];
                
                [dictionary setObject:calendar forKey:KEY_CALENDAR];
                [dictionary setObject:[[NSMutableArray alloc] init] forKey:KEY_EVENTS];
                
                if( [calendar ACLLink] )  // We can determine whether the calendar is under user's control by the existence of its edit link.
                    [dictionary setObject:KEY_EDITABLE forKey:KEY_EDITABLE];
                
                NSURL *feedURL = [[calendar alternateLink] URL];
                if( feedURL ){
                    GDataQueryCalendar* query = [GDataQueryCalendar calendarQueryWithFeedURL:feedURL];
                    
                    // Currently, the app just shows calendar entries from 15 days ago to 31 days from now.
                    // Ideally, we would instead use similar controls found in Google Calendar web interface, or even iCal's UI.
                    NSDate *minDate = [NSDate date];  // From right now...
                    NSDate *maxDate = [NSDate dateWithTimeIntervalSinceNow:60*60*24*90];  // ...to 90 days from now.
                    
                    [query setMinimumStartTime:[GDataDateTime dateTimeWithDate:minDate timeZone:[NSTimeZone systemTimeZone]]];
                    [query setMaximumStartTime:[GDataDateTime dateTimeWithDate:maxDate timeZone:[NSTimeZone systemTimeZone]]];
                    [query setOrderBy:@"starttime"];  // http://code.google.com/apis/calendar/docs/2.0/reference.html#Parameters
                    [query setIsAscendingOrder:YES];
                    [query setShouldExpandRecurrentEvents:YES];
                    
                    GDataServiceTicket *ticket = [googleCalendarService fetchFeedWithQuery:query
                                                                                  delegate:self
                                                                         didFinishSelector:@selector( eventsTicket:finishedWithEntries:error: )];
                    // I add the service ticket to the dictionary to make it easy to find which calendar each reply belongs to.
                    [dictionary setObject:ticket forKey:KEY_TICKET];
                }
            }
        }
    }else
        [self handleError:error];
    
    [self.tableView reloadData];
}

- (void)eventsTicket:(GDataServiceTicket *)ticket finishedWithEntries:(GDataFeedCalendarEvent *)feed error:(NSError *)error{
    if( !error )
    {
        NSMutableDictionary *dictionary;
        for( int section=0; section<[data count]; section++ )
        {
            NSMutableDictionary *nextDictionary = [data objectAtIndex:section];
            GDataServiceTicket *nextTicket = [nextDictionary objectForKey:KEY_TICKET];
            if( nextTicket==ticket )
            {		// We've found the calendar these events are meant for...
                dictionary = nextDictionary;
                break;
            }
        }
        
        if( !dictionary )
            return;		// This should never happen.  It means we couldn't find the ticket it relates to.
        
        int count = [[feed entries] count];
        
        NSMutableArray *events = [dictionary objectForKey:KEY_EVENTS];
        for( int i=0; i<count; i++ ){
            if (categorySelected) {
                if ([[[(GDataEntryCalendarEvent *)[[feed entries] objectAtIndex:i] content] stringValue] rangeOfString:[self.categories objectAtIndex:categoryIndex]].length) {
                    [events addObject:[[feed entries] objectAtIndex:i]];
                    NSLog( @"Selected catagory %d", categoryIndex);
                }
            }else{
                [events addObject:[[feed entries] objectAtIndex:i]];
            }
        
        }
        [self.tableView reloadData];
        
        NSURL *nextURL = [[feed nextLink] URL];
        if( nextURL ){    // There are more events in the calendar...  Fetch again.
            GDataServiceTicket *newTicket = [googleCalendarService fetchFeedWithURL:nextURL
                                                                           delegate:self
                                                                  didFinishSelector:@selector( eventsTicket:finishedWithEntries:error: )];   // Right back here...
            // Update the ticket in the dictionary for the next batch.
            [dictionary setObject:newTicket forKey:KEY_TICKET];
        }
    }else
        [self handleError:error];
}

- (void)deleteCalendarEvent:(GDataEntryCalendarEvent *)event{
    [googleCalendarService deleteEntry:event
                              delegate:self
                     didFinishSelector:nil];
}

- (void)insertCalendarEvent:(GDataEntryCalendarEvent *)event toCalendar:(GDataEntryCalendar *)calendar{
    [googleCalendarService fetchEntryByInsertingEntry:event
                                           forFeedURL:[[calendar alternateLink] URL]
                                             delegate:self
                                    didFinishSelector:@selector( insertTicket:finishedWithEntry:error: )];
}

- (void)insertTicket:(GDataServiceTicket *)ticket finishedWithEntry:(GDataEntryCalendarEvent *)entry error:(NSError *)error{
    if( !error )
        [self refresh];
    else
        [self handleError:error];
}

- (void)updateCalendarEvent:(GDataEntryCalendarEvent *)event{
    [googleCalendarService fetchEntryByUpdatingEntry:event
                                         forEntryURL:[[event editLink] URL]
                                            delegate:self
                                   didFinishSelector:nil];
}

#pragma mark Table Content and Appearance

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
#warning testing something out here... fix it later
//    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self.searchBar resignFirstResponder];
//    self.tableView.allowsSelection = YES;
    self.tableView.scrollEnabled = YES;
//    self.searchBar.text=@"";
//    [self.tableView setContentOffset:CGPointMake(0, self.searchBar.bounds.size.height-5) animated:YES];
    
//    isFiltered = FALSE;
//    [self.tableView reloadData];
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        [self.searchBar setShowsCancelButton:YES animated:YES];
        isFiltered = true;
        filteredTableData = [[NSMutableArray alloc] init];
        
        NSMutableDictionary *calander = [data objectAtIndex:0];
        
        for (GDataEntryCalendarEvent* event in [calander objectForKey:KEY_EVENTS])
        {
            NSRange nameRange = [[[event title] stringValue]  rangeOfString:text options:NSCaseInsensitiveSearch];
            NSRange descriptionRange = [[[[event locations] objectAtIndex:0] stringValue] rangeOfString:text options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound)
            {
                [filteredTableData addObject:event];
            }
        }
    }
    
    [self.tableView reloadData];
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if (isFiltered) {
        return 1;
    }
    return MAX( [data count], 1 );
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if( section<[data count] ){
        if (isFiltered) {
            int count = [filteredTableData count];
            return [NSString stringWithFormat:@"Search Result (%i)", count];
        } else {
        }
        NSMutableDictionary *dictionary = [data objectAtIndex:section];
        GDataEntryCalendar *calendar = [dictionary objectForKey:KEY_CALENDAR];
        NSMutableArray *events = [dictionary objectForKey:KEY_EVENTS];
        int count = [events count];
        //        NSLog(@" Calendar name: -> %@",[[calendar title] stringValue]);
        return [NSString stringWithFormat:@"%@ (%i)", [[calendar title] stringValue], count];
        
        
    }
    return @"Please wait...";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if( section>=[data count] )
        return 0;
    
    if (isFiltered) {
        return [filteredTableData count];
    }
    
    NSMutableDictionary *dictionary = [data objectAtIndex:section];
    NSMutableArray *events = [dictionary objectForKey:KEY_EVENTS];
    int count = [events count];
    
    return count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    static NSString *CellIdentifier = @"DetailCell";
    IITEventsCell *cell;// = (IITEventsCell *)[tableView :CellIdentifier];
//    if( !cell ){

        cell = [[IITEventsCell alloc] initWithFrame:CGRectMake(0, 0, lionCellWidth, lionCellHeight)];
        cell.accessoryType = UITableViewCellAccessoryNone;
//    }

    cell.date.text = cell.time.text = cell.name.text = cell.addr.text = @"";
    NSArray *events = [self eventsForIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
#warning this is where the images should be put back
    if (indexPath.row==0)
    cell.image.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ssv" ofType:@"jpeg"]];
    else if (indexPath.row==1) {
        cell.image.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"crown" ofType:@"jpeg"]];
    }else if (indexPath.row==2) {
        cell.image.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"mtcc" ofType:@"jpeg"]];
    }else if (indexPath.row==3) {
        cell.image.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"pool" ofType:@"jpeg"]];
    }else{
        cell.image.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ssv" ofType:@"jpeg"]];
    }
    
    // for now, put red background or something
    cell.backgroundColor = [UIColor blackColor];
    
    
    // The DetailCell has two modes of display - either the typical record or a prompt for creating a new item
    if( indexPath.row<[events count] ){
        
        GDataEntryCalendarEvent *event = [events objectAtIndex:indexPath.row];
        if (isFiltered) {
            event = [filteredTableData objectAtIndex:indexPath.row];
        }
        GDataWhen *when = [[event objectsForExtensionClass:[GDataWhen class]] objectAtIndex:0];
        if( when ){
            NSDate *date = [[when startTime] date];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            
            [dateFormatter setDateFormat:@"yy-MM-dd"];
            cell.date.text = [dateFormatter stringFromDate:date];
            [dateFormatter setDateFormat:@"HH:mm"];
            cell.time.text = [dateFormatter stringFromDate:date];
            
        }
        cell.name.text = [[event title] stringValue];
        // Note: An event might have multiple locations.  We're only displaying the first one.
        GDataWhere *addr = [[event locations] objectAtIndex:0];
        if( addr )
            cell.addr.text = [addr stringValue];
        
        cell.promptMode = NO;
        
        
        //check for tags
        NSString *tags = [[event content] stringValue];

        if ([tags rangeOfString:@"#education"].location == NSNotFound) {
            [cell.icon5 setHidden:YES];
        }if ([tags rangeOfString:@"#pro"].location == NSNotFound) {
            [cell.icon6 setHidden:YES];
        }if ([tags rangeOfString:@"#entertainment"].location == NSNotFound) {
            [cell.icon4 setHidden:YES];
        }if ([tags rangeOfString:@"#food"].location == NSNotFound) {
            [cell.icon2 setHidden:YES];
        }if ([tags rangeOfString:@"#prizes"].location == NSNotFound) {
            [cell.icon3 setHidden:YES];
        }if ([tags rangeOfString:@"#sports"].location == NSNotFound) {
            [cell.icon1 setHidden:YES];
        }
        CGRect frame = cell.frame;
        frame.size.height -=5;
        cell.frame = frame;
        
    }else{
        cell.prompt.text = @"Create new event";
        cell.promptMode = YES;
    }
    
    
        
    
    
    return cell;
}


#pragma mark Editing

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    //    NSLog(@"Object: %@",calanderEvent);
    if ([segue.identifier isEqualToString:@"detailView"]) {
        
        NSLog(@"Object being set : %@",calanderEvent);
        [segue.destinationViewController setEventInfo:calanderEvent];
    }
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    if( !self.editing || ![[self dictionaryForIndexPath:indexPath] objectForKey:KEY_EDITABLE] ){ // This will give the user visual feedback that the cell was selected but fade out to indicate that no action is taken.
    //        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    //        return;
    //    }
    
    calanderEvent=[[GDataEntryCalendarEvent alloc] init];
    calanderEvent=[self eventForIndexPath:indexPath];
    
    if (isFiltered) {
        calanderEvent = [filteredTableData objectAtIndex:indexPath.row];
    }
    
//    NSLog(@" creating event: %@",calanderEvent);
#warning uncomment when developing for ipad
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//        EventDetailViewController *detailVC= [self.splitViewController.viewControllers objectAtIndex:1];
//        [detailVC setEventInfo:calanderEvent];
//        [detailVC.view layoutSubviews];
//    }else {
        [self performSegueWithIdentifier:@"detailView" sender:self];
//    }
    
    
    // Don't maintain the selection. We will navigate to a new view so there's no reason to keep the selection here.
    //    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    //    [self editEntryAtIndexPath:indexPath];
}



- (void)viewDidUnload {
    [self setSearchBar:nil];
    [super viewDidUnload];
}
@end