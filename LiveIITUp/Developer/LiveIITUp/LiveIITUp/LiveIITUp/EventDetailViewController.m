//
//  EventDetailViewController.m
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/21/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#import "EventDetailViewController.h"
#import <EventKit/EventKit.h>

@interface EventDetailViewController ()

@end

@implementation EventDetailViewController
@synthesize navigationItem;
@synthesize eventLogo;
@synthesize eventTitle;
@synthesize chosenReminderTime;

@synthesize eventDate;
@synthesize eventOrganizer;
@synthesize eventLocation;
@synthesize infoIcon;
@synthesize eventInfo;
@synthesize addToCalButton;


-(void)setAlarm:(NSInteger)chosenTime{
    
    [self.addToCalButton setEnabled:NO];
    NSLog(@"I can print out the event info: %@",self.eventInfo);
    
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        NSLog(@"Access to calendar granted");
        /* This code will run when uses has made his/her choice */
        EKEvent *event  = [EKEvent eventWithEventStore:eventStore];
        event.title     = self.eventTitle.text;
        
        GDataWhen *when = [[self.eventInfo objectsForExtensionClass:[GDataWhen class]] objectAtIndex:0];
        
        event.startDate = [[when startTime] date];
        event.endDate   = [[when endTime] date];
        
        NSLog(@"ParentVC time interval: %d", chosenTime);
        if (!chosenTime){
            self.chosenReminderTime = -3600;
            NSLog(@"The chosen time is not set!");
//            chosenTime=self.chosenReminderTime;
        }
        
        event.alarms = [NSArray arrayWithObjects:[EKAlarm alarmWithRelativeOffset:self.chosenReminderTime], nil];
        NSLog(@"the alarm was set to: %@",event.alarms);
        
        [event setCalendar:[eventStore defaultCalendarForNewEvents]];
        NSError *err;
        [eventStore saveEvent:event span:EKSpanThisEvent error:&err];
    }];

    
}

- (IBAction)addToCalander:(id)sender {
    NSLog(@"I can print out the event info: %@",self.eventInfo);
    
    
    
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        NSLog(@"Access to calendar granted");
        /* This code will run when uses has made his/her choice */
        EKEvent *event  = [EKEvent eventWithEventStore:eventStore];
        event.title     = self.eventTitle.text;
        
        GDataWhen *when = [[self.eventInfo objectsForExtensionClass:[GDataWhen class]] objectAtIndex:0];
        
        event.startDate = [[when startTime] date];
        event.endDate   = [[when endTime] date];
        
        NSLog(@"ParentVC time interval: %d", self.chosenReminderTime);
        if (!self.chosenReminderTime)
            self.chosenReminderTime = -3600;
        
        event.alarms = [NSArray arrayWithObjects:[EKAlarm alarmWithRelativeOffset:self.chosenReminderTime], nil];
        NSLog(@"the alarm was set to: %@",event.alarms);
        
        [event setCalendar:[eventStore defaultCalendarForNewEvents]];
        NSError *err;
        [eventStore saveEvent:event span:EKSpanThisEvent error:&err];
    }];
    
    
    
    
}

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//        NSLog(@"print out event info: %@",self.eventInfo);
//
////        [self.eventOrganizer.text stringByAppendingFormat:[self.eventInfo description]];
//    }
//    return self;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.eventInfo) {
        GDataWhen *when = [[self.eventInfo objectsForExtensionClass:[GDataWhen class]] objectAtIndex:0];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        //        [dateFormat setDateStyle:NSDateFormatterMediumStyle];
        [dateFormat setDateFormat:@"EEE, MM/dd/yyyy hh:mma"];
        
        self.eventTitle.text=[[self.eventInfo title] stringValue];
//        self.eventOrganizer.text=[@"What: "  stringByAppendingFormat:@"%@",[self.eventInfo description] ];
        self.eventOrganizer.text=[@""  stringByAppendingFormat:@"%@",[[self.eventInfo content] stringValue]];
        self.eventLocation.text = [@"" stringByAppendingString:[[[self.eventInfo locations] objectAtIndex:0] stringValue]];
        self.eventDate.text =  [@"" stringByAppendingFormat:@"%@",[dateFormat stringFromDate:[[when startTime] date]]];
        self.eventLogo = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"mtcc" ofType:@"jpg"]]];
        
    }
    //    self.eventTitle.text=[[self.eventInfo title] stringValue];
    if (self.chosenReminderTime) {
        [self setAlarm:self.chosenReminderTime];
    }
    
    
	// Do any additional setup after loading the view.
}

-(void)viewDidLayoutSubviews{
    
    if (self.eventInfo) {
        GDataWhen *when = [[self.eventInfo objectsForExtensionClass:[GDataWhen class]] objectAtIndex:0];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
        //        [dateFormat setDateStyle:NSDateFormatterMediumStyle];
        [dateFormat setDateFormat:@"EEE, MMMM dd yyyy"];
        [dateFormat2 setDateFormat:@"hh:mma"];
        
        self.eventTitle.text=[[self.eventInfo title] stringValue];
        self.eventOrganizer.text=[@""  stringByAppendingFormat:@"%@",[[self.eventInfo content] stringValue]];
        
        
        
//        CGSize expectedLabelSize = [self.eventOrganizer.text sizeWithFont:self.eventOrganizer.font];
//        CGRect frame= self.eventOrganizer.frame;
//        frame.size = expectedLabelSize;
//        self.eventOrganizer.frame = frame;
//        self.scrollView.contentSize = expectedLabelSize;
        
//        NSLog(@"Description field %@", [[self.eventInfo content] stringValue]);
        
        
//        self.eventOrganizer.text=[@"What: "  stringByAppendingFormat:@"%@",[self.eventInfo description] ];
        self.eventLocation.text = [@"" stringByAppendingString:[[[self.eventInfo locations] objectAtIndex:0] stringValue]];
        self.eventDate.text =  [@"" stringByAppendingFormat:@"%@",[dateFormat stringFromDate:[[when startTime] date]]];
        self.eventTime.text=[@"" stringByAppendingFormat:@"%@",[dateFormat2 stringFromDate:[[when startTime] date]]];

        self.eventLogo = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"mtcc" ofType:@"jpg"]]];
//        self.infoIcon.image= [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"bookicon" ofType:@"png"]];
        
    }
    [self.view layoutSubviews];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sende{
    if (segue.identifier == @"setAlert") {
        [segue.destinationViewController setDelegate:self];
    }
}

- (void)viewDidUnload
{
    [self setNavigationItem:nil];
    [self setEventLogo:nil];
    [self setEventTitle:nil];
    [self setEventDate:nil];
    [self setEventOrganizer:nil];
    [self setInfoIcon:nil];
    [self setEventTitle:nil];
    [self setAddToCalButton:nil];
    [self setEventLocation:nil];
    [self setEventTime:nil];
    [self setScrollView:nil];
    [self setEventOrganizer:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end
