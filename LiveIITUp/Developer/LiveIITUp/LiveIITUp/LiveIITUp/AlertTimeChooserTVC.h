//
//  AlertTimeChoserTVC.h
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/20/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventDetailViewController.h"

@interface AlertTimeChooserTVC : UITableViewController<EventDetailViewControllerDelegate>

@property (strong, nonatomic) NSArray *times;
@property (strong, atomic) NSArray *displayValues;
@property (readwrite, nonatomic) int selectedTime;
@property (weak, nonatomic) id <EventDetailViewControllerDelegate> delegate;
-(void)setAlarm:(NSInteger)chosenTime;
@end
