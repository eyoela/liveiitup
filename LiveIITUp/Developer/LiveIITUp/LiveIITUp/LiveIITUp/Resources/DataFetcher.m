//
//  DataFetcher.m
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/21/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#import "DataFetcher.h"

@implementation DataFetcher

+(NSArray *)fetchArrayfromJsonFile:(NSString *)filePath
{
    NSData *dataFromFile = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:filePath ofType:@"json"]];
    NSError *errors;
    NSArray * arrayofValues=[NSJSONSerialization JSONObjectWithData:dataFromFile options:0 error:&errors];
    return arrayofValues;
}

@end