//
//  DataFetcher.h
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/21/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataFetcher : NSObject
+(NSArray *)fetchArrayfromJsonFile:(NSString *)filePath;
@end
