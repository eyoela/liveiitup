//
//  EventDetailViewController.h
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/21/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GData.h"

#import "GData.h"

@class EventDetailViewController;
@protocol EventDetailViewControllerDelegate <NSObject>
//-(void)setAlarm:(NSInteger)chosenTime;
@end

@interface EventDetailViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITextView *eventOrganizer;

@property (strong, nonatomic) IBOutlet UINavigationItem *navigationItem;
@property (strong, nonatomic) IBOutlet UILabel *eventTime;
@property (strong, atomic) IBOutlet UIImageView *eventLogo;
@property (strong, nonatomic) IBOutlet UILabel *eventTitle;
@property (strong, nonatomic) IBOutlet UILabel *eventDate;
@property (weak, nonatomic) IBOutlet UILabel *eventLocation;
@property (strong, nonatomic) IBOutlet UIImageView *infoIcon;
@property (readwrite, nonatomic) NSInteger chosenReminderTime;
@property (strong, nonatomic) GDataEntryCalendarEvent *eventInfo;

@property (weak, nonatomic) IBOutlet UIButton *addToCalButton;

-(void)setAlarm:(NSInteger)chosenTime;
@end