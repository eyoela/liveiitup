//
//  CategoriesTableVC.h
//  LiveIITUp
//
//  Created by Eyoel Berhane Asfaw on 10/23/12.
//  Copyright (c) 2012 Eyoel Berhane Asfaw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoriesTableVC : UITableViewController
@property (strong, atomic) NSArray *displayValues;
@property   (readwrite, nonatomic) NSInteger *categorySelected;
@end
