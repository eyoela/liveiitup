//
//  FeedObject.m
//  CS442Assignment1
//
//  Created by Eyoel Berhane Asfaw on 3/4/13.
//  Copyright (c) 2013 Eyoel Berhane Asfaw. All rights reserved.
//

#import "EventObject.h"

@implementation EventObject
@synthesize title=_title, type=_type, description=_description, link=_link, date=_date, elements=_elements;

-(NSString *)description{
    NSString    *toString;
        
    toString=_elements[@"content"];
    
    return [toString substringFromIndex:[toString rangeOfString:@"Event Description:"].location];
}
-(NSString *)startDate{
    
    if (_elements[@"pubDate"])
        return _elements[@"pubDate"];
    else if (_elements[@"published"])
        return _elements[@"published"];
    else
        return @"Unknown Date";
}
-(NSString *)title{
    if (_elements[@"title"])
        return _elements[@"title"];
    else
        return @"No title";
}

@end
